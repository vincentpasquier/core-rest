/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.rest.tests.sesame;

import org.junit.After;
import org.junit.Before;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.ObjectRepository;
import org.openrdf.repository.object.config.ObjectRepositoryFactory;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class GenericSesameTest {

	public Repository store;

	public ObjectRepositoryFactory factory;

	public ObjectRepository repository;

	public ObjectConnection connection;

	public ValueFactory vf;

	@Before
	public void setupRepository () throws RepositoryException, RepositoryConfigException {
		//store = new SailRepository ( new MemoryStore () );
		store = new HTTPRepository ( "http://predict.es/openrdf-sesame/", "predict" );
		store.initialize ();
		factory = new ObjectRepositoryFactory ();
		repository = factory.createRepository ( store );
		connection = repository.getConnection ();
		vf = repository.getValueFactory ();
	}

	@After
	public void clearRepository () throws RepositoryException {
		connection.close ();
		store.shutDown ();
	}
}
