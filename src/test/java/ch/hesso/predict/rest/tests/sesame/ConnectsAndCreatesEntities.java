/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.rest.tests.sesame;

import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.Cfp;
import javassist.CannotCompileException;
import javassist.NotFoundException;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.ObjectRepository;
import org.openrdf.repository.object.config.ObjectRepositoryFactory;

import java.io.IOException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ConnectsAndCreatesEntities {

	public static void main ( final String args[] ) throws RepositoryException, RepositoryConfigException, QueryEvaluationException, NotFoundException, IOException, CannotCompileException, ClassNotFoundException {
		Repository store = new HTTPRepository ( "http://predict.es/openrdf-sesame/", "predict" );
		store.initialize ();
		ObjectRepositoryFactory factory = new ObjectRepositoryFactory ();
		ObjectRepository repository = factory.createRepository ( store );

		/*Conference.Builder confBuilder = Conference.Builder.newBuilder ( "BLU" );
		confBuilder.person ( "Addi" );

		Conference conf = confBuilder.build ();*/

		ObjectConnection con = repository.getConnection ();
		ValueFactory vf = con.getValueFactory ();
		String uri = APIEntity.BASE_API_URL + "/calls/dsaklajldka";
		System.out.println ( uri );
		URI id = vf.createURI ( uri );
		con.commit ();

		Cfp resolve = con.getObject ( Cfp.class, id );
		System.out.println ( resolve );
		System.out.println ( resolve.getConferenceId () );

		/*ClassPool pool = ClassPool.getDefault ();
		CtClass ctCoder = pool.getCtClass ( "ch.hesso.predict.entities.JSONCoder" );
		//CtClass cfpClass = pool.get ( "ch.hesso.predict.entities.CFP$CFPCoder" );
		//System.out.println ( cfpClass.getGenericSignature () );

		//System.out.println ( ctCoder );
		CtClass blu = pool.makeClass ( "BluCoder", ctCoder );
		blu.setGenericSignature ( "Lch/hesso/predict/entities/JSONCoder<Lch/hesso/predict/entities/CFP;>;" );
		//System.out.println ( cfpClass );
		//System.out.println ( blu );
		ClassFile ccfile = blu.getClassFile ();
		ConstPool constPool = ccfile.getConstPool ();
		Annotation annotation = new Annotation ( ServerEndpoint.class.getCanonicalName (), constPool );
		annotation.addMemberValue ( "value", new StringMemberValue ( "/cfp", constPool ) );
		AnnotationsAttribute attr = new AnnotationsAttribute ( constPool, AnnotationsAttribute.visibleTag );
		attr.addAnnotation ( annotation );
		ccfile.addAttribute ( attr );
		Class<?> bluCoder = blu.toClass ();

		ParameterizedType $class = (ParameterizedType) bluCoder.getGenericSuperclass ();
		Type $type = $class.getActualTypeArguments ()[ 0 ];
		System.out.println ( $type );
		if ( $type instanceof Class ) {
			// SW: $type is indeed a class instance
			System.out.println ( "instanceof" );
		} else if ( $type instanceof ParameterizedType ) {
			// SW: $type is indeed a ParametrizedType
			// SW: also, getting a Raw Type wield a Class of type T
			System.out.println ( ( ( (ParameterizedType) $type ).getRawType () ) );
		}
		for ( java.lang.annotation.Annotation ano : bluCoder.getAnnotations () ) {
			System.out.println ( ano );
		}*/
	}
}
