/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.rest.tests.sesame;

import ch.hesso.predict.storage.PersonBean;
import ch.hesso.predict.storage.URIFactory;
import org.junit.Assert;
import org.junit.Test;
import org.openrdf.model.URI;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;

import java.util.UUID;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class AuthorSesameTest extends GenericSesameTest {

	/*@Test
	public void createAuthor () throws RepositoryException, QueryEvaluationException {
		AuthorBean author = SesameEntityFactory.genericAuthor ();
		URI uri = vf.createURI ( AuthorBean.NS, author.getId () );
		connection.addObject ( uri, author );

		AuthorBean same = connection.getObject ( AuthorBean.class, uri );
		Assert.assertTrue ( author.getId ().equals ( same.getId () ) );
		Assert.assertTrue ( author.getName ().equals ( same.getName () ) );
		Assert.assertTrue ( author.getCountry ().equals ( same.getCountry () ) );
		// finish asserts
	}*/

	//@Test
	public void createAuthorWithPublication () throws RepositoryException, QueryEvaluationException {
		PersonBean author = SesameEntityFactory.genericAuthor ();
		URI uriAuthor = URIFactory.PERSON.createURI ( connection.getValueFactory (), author );
		connection.addObject ( uriAuthor, author );

		PersonBean same = connection.getObject ( PersonBean.class, uriAuthor );
		Assert.assertTrue ( author.getId ().equals ( same.getId () ) );
		Assert.assertTrue ( author.getName ().equals ( same.getName () ) );
		Assert.assertTrue ( author.getCountry ().equals ( same.getCountry () ) );
		Assert.assertTrue ( author.getPublications ().size () == 0 );
	}

	@Test
	public void createProgramCommittee () throws RepositoryException, QueryEvaluationException {
		PersonBean pc = SesameEntityFactory.genericAuthor ();
		UUID uuid = UUID.randomUUID ();
		String id = uuid.toString ().replaceAll ( "-", "" );
		System.out.println (id);
	}

	/*@Test
	public void testQueryAuthors () throws RepositoryException, MalformedQueryException, QueryEvaluationException {
		ObjectQuery query = connection.prepareObjectQuery (
				"PREFIX aut:<http://predict.es/vocab/person/author#>\n" +
						"SELECT ?author WHERE { ?author ?x aut:author }\n" +
						"ORDER BY ?author\n" +
						"LIMIT 1\n" +
						"OFFSET 1" );
		Result<AuthorBean> authors = query.evaluate ( AuthorBean.class );
		for ( AuthorBean author : authors.asSet () ) {
			System.out.println ( author.getPublications ().size () );
			System.out.println ( author.getName () );
		}
	}*/

}
