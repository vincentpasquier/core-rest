/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.rest.tests.sesame;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.query.*;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.ObjectRepository;
import org.openrdf.repository.object.config.ObjectRepositoryFactory;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.nativerdf.NativeStore;

import java.io.File;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class SelectAbstractTests {

	static Repository repository
			= new SailRepository ( new NativeStore ( new File ( "/mnt/Damocles/master/datastore" ) ) );

	@BeforeClass
	public static void before () throws RepositoryException {
		repository.initialize ();
	}

	@Test
	public void testSelect () throws RepositoryException, MalformedQueryException, QueryEvaluationException, RepositoryConfigException {
		final ObjectRepositoryFactory _factory = new ObjectRepositoryFactory ();
		ObjectRepository _repository = _factory.createRepository ( repository );

		ObjectConnection connection = _repository.getConnection ();

		/*ObjectQuery query = connection.prepareObjectQuery ( QueryLanguage.SPARQL,
				StorageEntity.SHORT_NS + "SELECT ?conf WHERE\n" +
						"{ ?conf conf:keywords ?keyword . ?conf conf:h-index ?hindex" +
						" }\n" +
						"ORDER BY ?hindex" );
		query.setBinding ( "keyword", connection.getValueFactory ().createLiteral ( "Graphics" ) );

		Set<ConferenceBean> conferenceBeans = query.evaluate ( ConferenceBean.class ).asSet ();
		System.out.println (conferenceBeans.size ());
		query.setBinding ( "keyword", connection.getValueFactory ().createLiteral ( "Computer Vision" ) );
		Set<ConferenceBean> conferencesCV = query.evaluate ( ConferenceBean.class ).asSet ();
		System.out.println (conferencesCV.size ());
		conferenceBeans.addAll ( conferenceBeans );
		System.out.println (conferenceBeans.size ());*/

		//Query query = connection.prepareQuery ( "SELECT (COUNT(*) AS ?no) { ?s ?p ?o  }" );
		TupleQuery query = connection.prepareTupleQuery ( "PREFIX per:<http://predict.es/vocab/person#>\n" +
				"SELECT ?s { ?s ?p per:person  }" );

		TupleQueryResult res = query.evaluate ();
		long count = 0;
		while ( res.hasNext () ) {
			res.next ();
			count++;
		}
		System.out.println (count);

		connection.close ();

		/*TupleQuery queryConference = connection.prepareTupleQuery ( QueryLanguage.SPARQL,
				StorageEntity.SHORT_NS
						+ "SELECT ?x \n"
						+ "WHERE { "
						+ "?x conf:key ?confKey"
						+ " . "
						+ "?x ent:id ?confId"
						+ " }\n"
						+ "LIMIT 10\n" );*/
		//queryConference.setBinding (  );

		/*TupleQuery queryVenue = connection.prepareTupleQuery ( QueryLanguage.SPARQL,
				StorageEntity.SHORT_NS
						+ "SELECT ?title ?venueId \n"
						+ "WHERE { "
						+ "?obj pub:title ?title"
						+ " . "
						+ "?obj pub:venue ?venueId"
						+ " }\n"
						+ "LIMIT 10" );*/


		/*TupleQueryResult resultsConferences = queryConference.evaluate ();
		while ( resultsConferences.hasNext () ) {
			BindingSet conferences = resultsConferences.next ();
			System.out.println ( conferences );
		}

		resultsConferences.close ();*/

		/*TupleQueryResult results = queryVenue.evaluate ();
		while ( results.hasNext () ) {
			BindingSet set = results.next ();
			Value title = set.getValue ( "title" );
			Value venueId = set.getValue ( "venueId" );
			Value confId = set.getValue ( "confId" );
			System.out.println ( title.stringValue () );
			System.out.println ( venueId.stringValue () );
			System.out.println ( confId );
		}
		results.close ();*/

	}

	@AfterClass
	public static void after () throws RepositoryException {
		repository.shutDown ();
	}

}
