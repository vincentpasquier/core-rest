/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.rest.tests.sesame;

import ch.hesso.predict.storage.CfpBean;
import ch.hesso.predict.storage.PersonBean;
import ch.hesso.predict.storage.PublicationBean;
import ch.hesso.predict.storage.VenueBean;
import com.google.common.collect.ImmutableSet;

import java.util.Arrays;
import java.util.HashSet;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class SesameEntityFactory {

	public static PersonBean genericAuthor () {
		PersonBean author = new PersonBean ();
		String someId = "some-id";
		author.setId ( someId );
		author.setName ( "Vincent Pasquier" );
		author.setCountry ( "Some country" );
		author.setEstablishments ( new HashSet<> ( Arrays.asList ( "Something", "Some more" ) ) );
		author.setTitle ( "GeF" );
		return author;
	}

	public static PublicationBean genericPublication () {
		PublicationBean publication = new PublicationBean ();
		publication.setId ( "some-key-id" );
		publication.setTitle ( "TITLEEEEE" );
		publication.setDoi ( "10-doidoi1010/10" );
		publication.setYear ( 2010 );
		return publication;
	}

	public static CfpBean genericCFP () {
		CfpBean cfp = new CfpBean ();
		cfp.setId ( "yeah-id" );
		cfp.setRelated ( new HashSet<> ( Arrays.asList ( "AI", "conference" ) ) );
		cfp.setContent ( "some content, super super super super super super long .... can you fit ? is it still okay to put content ? hope so :)" +
				"Some more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more text" +
				"Some more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more text" +
				"Some more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more textSome more text" );
		cfp.setOrigin ( "cfp" );
		cfp.setWebsite ( "asdf" );
		return cfp;
	}

	public static VenueBean genericVenue () {
		VenueBean venue = new VenueBean ();
		venue.setId ( "asdf" );
		venue.setLocation ( ImmutableSet.of ( "Chez toi" ) );
		venue.setYear ( 2070 );
		return venue;
	}
}
