/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.rest.tests.sesame;

import ch.hesso.predict.repository.RepositoryFactory;
import ch.hesso.predict.repository.SesameRepository;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.storage.CfpBean;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static ch.hesso.predict.repository.SesameRepository.LIMIT;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class CFPSesameGetAllTest {

	public static final RepositoryFactory<CfpBean> CFP_FACADE
			= RepositoryFactory.newFactory ( CfpBean.class );

	@Test
	public void getAll () {

		int limit = 100;
		List<Cfp> toReturn = new ArrayList<> ();
		for ( int i = 0; i < 100000; i++ ) {
			int offset = LIMIT * i;
			SesameRepository.SesameResponse<CfpBean> cfps =
					CFP_FACADE.repository ().getAllLimitOffset ( offset, limit, "cfp:cfp", "" );

			for ( CfpBean cfpBean : cfps.getCollection () ) {
				Cfp cfp = new Cfp ();
				cfp.setId ( cfpBean.getId () );
				cfp.setDeadlines ( cfpBean.getDeadlines () );
				cfp.setReferrers ( cfpBean.getReferrers () );
				cfp.setKeywords ( cfpBean.getKeywords () );
				cfp.setRelated ( cfpBean.getRelated () );
				cfp.setOrigin ( cfpBean.getOrigin () );
				cfp.setWebsite ( cfpBean.getWebsite () );
				cfp.setTitle ( cfpBean.getTitle () );
				cfp.setAcronym ( cfpBean.getAcronym () );
				cfp.setStart ( cfpBean.getStart () );
				cfp.setEnd ( cfpBean.getEnd () );
				cfp.setLocation ( cfpBean.getLocation () );
				System.out.println ( "Added" );
				toReturn.add ( cfp );
			}

			if ( cfps.getCollection ().size () < LIMIT ) {
				break;
			}
		}
	}
}
