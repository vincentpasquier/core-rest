/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.rest.tests.sesame;

import ch.hesso.predict.storage.CfpBean;
import ch.hesso.predict.storage.ConferenceBean;
import ch.hesso.predict.storage.URIFactory;
import ch.hesso.predict.storage.VenueBean;
import org.junit.Test;
import org.openrdf.model.URI;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectQuery;

import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class CFPSesameTest extends GenericSesameTest {

	//@Test
	public void createCFP () throws RepositoryException {
		CfpBean cfp = SesameEntityFactory.genericCFP ();
		URI uriCFP = URIFactory.CFP.createURI ( vf, cfp );
		connection.addObject ( uriCFP, cfp );
	}

	//@Test
	public void selectCFP () throws RepositoryException, MalformedQueryException, QueryEvaluationException {
		int LIMIT = 50;
		int offset = 0;
		String queryString =
				"PREFIX cfp:<http://predict.es/vocab/conference/cfp#>" +
						"SELECT ?call WHERE { ?call ?x cfp:cfp }\n" +
						"ORDER BY ?call\n" +
						"LIMIT " + LIMIT + "\n" +
						"OFFSET " + offset;
		ObjectQuery query = connection.prepareObjectQuery ( queryString );

		Set<CfpBean> entities = query.evaluate ( CfpBean.class ).asSet ();
		for ( CfpBean bean : entities ) {
			System.out.println ( bean.getId () );
		}
		System.out.println ( entities.size () );
	}

	@Test
	public void addCFPConferenceVenue () throws QueryEvaluationException, RepositoryException {
		CfpBean cfp = SesameEntityFactory.genericCFP ();
		URI uriCFP = URIFactory.CFP.createURI ( vf, cfp );
		connection.addObject ( uriCFP, cfp );

		URI uriConf = URIFactory.CONFERENCE.createURIWithId ( vf, "blu" );
		ConferenceBean conf = new ConferenceBean ();
		conf.setId ( "blu" );
		connection.addObject ( uriConf, conf );

		URI uriVenue = URIFactory.VENUE.createURIWithId ( vf, "blo" );
		VenueBean v = new VenueBean ();
		v.setId ( "blo" );
		connection.addObject ( uriVenue, v );

		CfpBean sameCfp = connection.getObject ( CfpBean.class, uriCFP );
		ConferenceBean sameConf = connection.getObject ( ConferenceBean.class, uriConf );
		VenueBean sameVenue = connection.getObject ( VenueBean.class, uriVenue );
		sameCfp.addConference ( sameConf );
		sameVenue.addConference ( sameConf );
	}

}
