/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.servlet;


import ch.hesso.predict.repository.RepositoryFactory;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.predict.storage.*;
import ch.hesso.predict.visitors.StoragePostVisitor;
import ch.hesso.predict.visitors.WSPublishVisitor;
import ch.hesso.reflect.TopLevelClassFinder;
import ch.hesso.websocket.server.WebSocketManager;
import ch.hesso.websocket.server.WebSocketServer;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ServletLoading implements ServletContextListener {

	public static final RepositoryFactory<CfpBean> CFP_FACADE
			= RepositoryFactory.newFactory ( CfpBean.class );

	public static final RepositoryFactory<ConferenceBean> CONFERENCE_FACADE
			= RepositoryFactory.newFactory ( ConferenceBean.class );

	public static final RepositoryFactory<PersonBean> PERSON_FACADE
			= RepositoryFactory.newFactory ( PersonBean.class );

	public static final RepositoryFactory<PublicationBean> PUBLICATION_FACADE
			= RepositoryFactory.newFactory ( PublicationBean.class );

	public static final RepositoryFactory<VenueBean> VENUE_FACADE
			= RepositoryFactory.newFactory ( VenueBean.class );

	public static final RepositoryFactory<String> SESSION_FACADE
			= RepositoryFactory.newFactory ( String.class );

	public static final ConcurrentMap<String, ObjectConnection> CONNECTIONS = new ConcurrentHashMap<> ();

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( ServletLoading.class );

	//
	private final Set<APIEntityVisitor> _visitors = new HashSet<> ();

	//
	private WebSocketManager _manager;

	/**
	 * Server Hostname, TODO look at System.getProperty ()
	 */
	public static final String WSSERVER_HOSTNAME = "localhost";

	/**
	 * Server port, TODO look at System.getProperty ()
	 */
	public static final int WSSERVER_PORT = 8025;

	/**
	 * Server path
	 */
	public static final String WSSERVER_PATH = "/";

	//
	private WebSocketServer server;

	/**
	 * @param sce
	 */
	@Override
	public void contextInitialized ( final ServletContextEvent sce ) {
		TopLevelClassFinder<APIEntity> finder = new TopLevelClassFinder<> ();
		Set<Class<? extends APIEntity>> tmpEndpoints = finder.findInstantiableSubClasses ( APIEntity.class, "ch.hesso.predict" );
		Set<Class<?>> endpoints = new HashSet<> ();
		for ( Class<?> tmpEndpoint : tmpEndpoints ) {
			endpoints.add ( tmpEndpoint );
		}
		WebSocketServer.Builder builder = WebSocketServer.Builder.create ( endpoints );
		builder.hostname ( WSSERVER_HOSTNAME );
		builder.port ( WSSERVER_PORT );
		builder.path ( WSSERVER_PATH );
		server = builder.build ();
		server.start ();
		initVisitors ();
		/*TopLevelClassFinder<Api> finder = new TopLevelClassFinder<> ();
		finder.findAnnotatedClasses ( Api.class, "ch" );*/
	}

	//
	private void initVisitors () {
		WSPublishVisitor wsPublisher = new WSPublishVisitor ();
		StoragePostVisitor storageVisitor = new StoragePostVisitor ();
		_visitors.add ( wsPublisher );
		_visitors.add ( storageVisitor );
	}

	/**
	 * @param sce
	 */
	@Override
	public void contextDestroyed ( final ServletContextEvent sce ) {
		RepositoryFactory.stop();
		server.stop ();
	}

}
