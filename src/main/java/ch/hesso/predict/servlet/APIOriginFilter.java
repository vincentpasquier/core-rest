/**
 *  Copyright 2013 Wordnik, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 */
public class APIOriginFilter implements javax.servlet.Filter {

	/**
	 * @param request
	 * @param response
	 * @param chain
	 *
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	public void doFilter ( final ServletRequest request,
												 final ServletResponse response,
												 final FilterChain chain ) throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse) response;
		res.addHeader ( "Access-Control-Allow-Origin", "*" );
		res.addHeader ( "Access-Control-Allow-Methods", "GET, POST, DELETE, PUT" );
		res.addHeader ( "Access-Control-Allow-Headers", "Content-Type" );
		chain.doFilter ( request, response );
	}

	/**
	 *
	 */
	@Override
	public void destroy () {
	}

	/**
	 * @param filterConfig
	 *
	 * @throws ServletException
	 */
	@Override
	public void init ( final FilterConfig filterConfig ) throws ServletException {
	}
}