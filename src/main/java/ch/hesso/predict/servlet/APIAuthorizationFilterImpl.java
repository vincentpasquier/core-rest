/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.servlet;

import com.wordnik.swagger.core.filter.SwaggerSpecFilter;
import com.wordnik.swagger.model.ApiDescription;
import com.wordnik.swagger.model.Operation;
import com.wordnik.swagger.model.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * The rules are maintained in simple map with key as path and a boolean value indicating given path is secure or not.
 * For method level security the key is combination of http method and path .
 * <p/>
 * If the resource or method is secure then it can only be viewed using a secured api key
 * <p/>
 * Note: Objective of this class is not to provide fully functional implementation of authorization filter. This is only
 * a sample demonstration how API authorization filter works.
 */
public class APIAuthorizationFilterImpl implements SwaggerSpecFilter {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( APIAuthorizationFilterImpl.class );

	/**
	 * @param operation
	 * @param api
	 * @param params
	 * @param cookies
	 * @param headers
	 *
	 * @return
	 */
	public boolean isOperationAllowed ( final Operation operation,
																			final ApiDescription api,
																			final Map<String, List<String>> params,
																			final Map<String, String> cookies,
																			final Map<String, List<String>> headers ) {
		boolean isAuthorized = checkKey ( params, headers );
		if ( isAuthorized ) {
			return true;
		} else {
			if ( operation.method () != "GET" || api.path ().indexOf ( "/store" ) != -1 ) {
				return true;
			} else {
				return true;
			}
		}
	}

	/**
	 * @param parameter
	 * @param operation
	 * @param api
	 * @param params
	 * @param cookies
	 * @param headers
	 *
	 * @return
	 */
	public boolean isParamAllowed ( final Parameter parameter,
																	final Operation operation,
																	final ApiDescription api,
																	final Map<String, List<String>> params,
																	final Map<String, String> cookies,
																	final Map<String, List<String>> headers ) {
		boolean isAuthorized = checkKey ( params, headers );
		if ( ( parameter.paramAccess ().isDefined () && parameter.paramAccess ().get ().equals ( "internal" ) ) && !isAuthorized ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @param params
	 * @param headers
	 *
	 * @return
	 */
	public boolean checkKey ( final Map<String, List<String>> params,
														final Map<String, List<String>> headers ) {
		String keyValue = null;
		if ( params.containsKey ( "api_key" ) )
			keyValue = params.get ( "api_key" ).get ( 0 );
		else {
			if ( headers.containsKey ( "api_key" ) )
				keyValue = headers.get ( "api_key" ).get ( 0 );
		}
		if ( "special-key".equals ( keyValue ) ) {
			return true;
		} else {
			return false;
		}
	}
}