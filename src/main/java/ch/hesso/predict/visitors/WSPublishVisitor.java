/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.visitors;

import ch.hesso.commons.AbstractEntity;
import ch.hesso.predict.events.RESTEvents;
import ch.hesso.predict.events.api.AbstractAPIEvent;
import ch.hesso.predict.restful.*;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.server.WebSocketManager;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Session;
import java.util.Set;


/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class WSPublishVisitor implements APIEntityVisitor {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( WSPublishVisitor.class );

	/**
	 *
	 */
	public WSPublishVisitor () {
		RESTEvents.BUS.register ( this );
	}

	/**
	 * @param sessions
	 * @param entity
	 */
	private void publish ( final Set<Session> sessions, final AbstractEntity entity ) {
		for ( Session session : sessions ) {
			session.getAsyncRemote ().sendObject ( entity );
		}
	}

	/**
	 * @param event
	 */
	@Subscribe
	public void onAPIEvent ( final AbstractAPIEvent<? extends APIEntity> event ) {
		APIEntity apiEntity = event.entity ();
		if ( !apiEntity.isBulk () ) {
			event.entity ().accept ( this );
		} else {
			LOG.debug ( String.format ( "%s has bulk flag, no publish", event.entity ().getClass ().getSimpleName () ) );
		}

	}

	@Override
	public void visit ( final Cfp cfp ) {
		Set<Session> sessions = WebSocketManager.sessions ( Cfp.ROOT_NAME, cfp.getMethod () );
		publish ( sessions, cfp );
	}

	@Override
	public void visit ( final Conference conference ) {
		Set<Session> sessions = WebSocketManager.sessions ( Conference.ROOT_NAME, conference.getMethod () );
		publish ( sessions, conference );
	}

	@Override
	public void visit ( final Doi doi ) {
		Set<Session> sessions = WebSocketManager.sessions ( Doi.ROOT_NAME, doi.getMethod () );
		publish ( sessions, doi );
	}

	@Override
	public void visit ( final Person person ) {
		Set<Session> sessions = WebSocketManager.sessions ( Person.ROOT_NAME, person.getMethod () );
		publish ( sessions, person );
	}

	@Override
	public void visit ( final Publication publication ) {
		Set<Session> sessions = WebSocketManager.sessions ( Publication.ROOT_NAME, publication.getMethod () );
		publish ( sessions, publication );
	}

	@Override
	public void visit ( final Ranking ranking ) {
		Set<Session> sessions = WebSocketManager.sessions ( Ranking.ROOT_NAME, ranking.getMethod () );
		publish ( sessions, ranking );
	}

	@Override
	public void visit ( final Venue venue ) {
		Set<Session> sessions = WebSocketManager.sessions ( Ranking.ROOT_NAME, venue.getMethod () );
		publish ( sessions, venue );
	}

	@Override
	public void visit ( final Plugin plugin ) {
		Set<Session> sessions = WebSocketManager.sessions ( Plugin.ROOT_NAME, plugin.getMethod () );
		publish ( sessions, plugin );
	}

	@Override
	public void visit ( final WebPage webPage ) {

	}

}
