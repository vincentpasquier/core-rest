/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.visitors;

import ch.hesso.predict.events.RESTEvents;
import ch.hesso.predict.events.api.APICreateEvent;
import ch.hesso.predict.restful.*;
import ch.hesso.predict.restful.visitors.APIEntityVisitable;
import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.*;
import com.google.common.eventbus.Subscribe;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class StoragePostVisitor extends AbstractStorageVisitor {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( StoragePostVisitor.class );

	/**
	 *
	 */
	public StoragePostVisitor () {
		RESTEvents.BUS.register ( this );
	}

	@Subscribe
	public void onCreateEvent ( final APICreateEvent<? extends APIEntityVisitable> event ) {
		event.entity ().accept ( this );
	}

	@Override
	public void visit ( final Cfp cfp ) {
		try {
			ObjectConnection connection =
					ServletLoading.CFP_FACADE.repository ().connection ( cfp.getSessionId () );
			CfpBean bean = Transfer.CFP_TRANSFER.fromTransferObject ( cfp );
			URI uri = URIFactory.CFP.createURI ( connection.getValueFactory (), bean );
			connection.addObject ( uri, bean );
		} catch ( RepositoryException e ) {
			LOG.debug ( String.format ( "Error while adding CFP [%s], check logs", cfp.getWebsite () ), e );
		}
	}

	@Override
	public void visit ( final Conference conference ) {
		try {
			ObjectConnection connection =
					ServletLoading.CONFERENCE_FACADE.repository ().connection ( conference.getSessionId () );
			ConferenceBean bean = Transfer.CONFERENCE_TRANSFER.fromTransferObject ( conference );
			URI uri = URIFactory.CONFERENCE.createURI ( connection.getValueFactory (), bean );
			connection.addObject ( uri, bean );
		} catch ( RepositoryException e ) {
			LOG.debug ( String.format ( "Error while adding conference [%s], check logs", conference.getKey () ), e );
		}
	}

	@Override
	public void visit ( final Doi doi ) {

	}

	@Override
	public void visit ( final Person person ) {
		try {
			ObjectConnection connection =
					ServletLoading.PERSON_FACADE.repository ().connection ( person.getSessionId () );
			PersonBean bean = Transfer.PERSON_TRANSFER.fromTransferObject ( person );
			URI uri = URIFactory.PERSON.createURI ( connection.getValueFactory (), bean );
			connection.addObject ( uri, bean );
		} catch ( RepositoryException e ) {
			LOG.debug ( String.format ( "Error while adding person [%s], check logs", person.getName () ), e );
		}
	}

	@Override
	public void visit ( final Publication publication ) {
		try {
			ObjectConnection connection =
					ServletLoading.PUBLICATION_FACADE.repository ().connection ( publication.getSessionId () );
			PublicationBean bean = Transfer.PUBLICATION_TRANSFER.fromTransferObject ( publication );
			URI uri = URIFactory.PUBLICATION.createURI ( connection.getValueFactory (), bean );
			connection.addObject ( uri, bean );
		} catch ( RepositoryException e ) {
			LOG.debug ( String.format ( "Error while adding publication [%s], check logs", publication.getKey () ), e );
		}
	}

	@Override
	public void visit ( final Ranking ranking ) {

	}

	@Override
	public void visit ( final Venue venue ) {
		try {
			ObjectConnection connection =
					ServletLoading.VENUE_FACADE.repository ().connection ( venue.getSessionId () );
			VenueBean bean = Transfer.VENUE_TRANSFER.fromTransferObject ( venue );
			URI uri = URIFactory.VENUE.createURI ( connection.getValueFactory (), bean );
			connection.addObject ( uri, bean );
		} catch ( RepositoryException e ) {
			LOG.debug ( String.format ( "Error while adding publication [%s], check logs", venue.getKey () ), e );
		}
	}

	@Override
	public void visit ( final Plugin plugin ) {

	}

	@Override
	public void visit ( final WebPage webPage ) {

	}

}
