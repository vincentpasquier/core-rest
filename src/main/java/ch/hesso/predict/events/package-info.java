/**
 * Provide event-based objects which are sent and received via {@link com.google.common.eventbus.EventBus}.
 *
 * Each event is a sub-class of {@link ch.hesso.predict.events.PredictEvent} which can be subscribed to via Guava's annotation {@link com.google.common.eventbus.Subscribe}.
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
package ch.hesso.predict.events;