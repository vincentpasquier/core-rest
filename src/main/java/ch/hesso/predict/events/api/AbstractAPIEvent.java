/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.events.api;

import ch.hesso.commons.AbstractEntity;
import ch.hesso.predict.events.PredictEvent;
import ch.hesso.predict.restful.APIEntity;

/**
 * Abstract RESTful API event. Holds a reference to the concerned entity which can be accessed.
 * <p/>
 * Used by Guava {@link com.google.common.eventbus.EventBus} to carry entities in server-side with event-based
 * programming.
 *
 * @param <T> parametrized modification, must be a subclass of {@code AbstractEntity}
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public abstract class AbstractAPIEvent<T extends APIEntity> extends PredictEvent {

	// Reference to concerned entity coming from API call
	private final T _entity;

	/**
	 * Initializes an API Event with corresponding entity.
	 *
	 * @param entity created/modified/deleted entity
	 */
	protected AbstractAPIEvent ( final T entity ) {
		_entity = entity;
	}

	/**
	 * @return the entity which is concerned by API call
	 */
	public T entity () {
		return _entity;
	}

}
