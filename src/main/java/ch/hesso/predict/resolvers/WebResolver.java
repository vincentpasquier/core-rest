/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resolvers;

import ch.hesso.cache.WebCache;
import ch.hesso.cache.exceptions.CacheIOException;
import ch.hesso.cache.exceptions.CacheQueryException;
import ch.hesso.cache.query.CacheQuery;
import ch.hesso.predict.restful.WebPage;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.HTTP;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class WebResolver extends GenericResolver {

	private final WebCache cache;

	public WebResolver () {
		WebCache.Builder webBuilder = WebCache.Builder.newBuilder ( new File ( "/mnt/Damocles/master/webcache" ) );
		webBuilder.partitions ( 503 );
		cache = webBuilder.build ();
	}

	public WebPage page ( final WebPage webPage ) throws CacheQueryException, WebApplicationException, IOException, CacheIOException {
		CacheQuery<WebPage> query = cache.newQuery ( webPage.getUrl () ).execute ();
		// Query returns result
		if ( query.hasMore () ) {
			WebPage page = query.next ();
			return page;
			// No results
		} else {
			// Is it to add ?
			if ( webPage.getContent () != null ) {
				if ( cache.cache ( webPage ) ) {
					return webPage;
				} else {
					throw new CacheIOException ( "Unable to save!" );
				}
				// It is to get http content
			} else {
				HttpGet resolve = new HttpGet ( webPage.getUrl () );
				resolve.addHeader ( HTTP.USER_AGENT, USER_AGENTS.get ( RANDOM.nextInt ( USER_AGENTS.size () ) ) );

				try {
					HttpResponse response = WEB_CLIENT.execute ( resolve );
					int responseCode = response.getStatusLine ().getStatusCode ();
					if ( responseCode != HttpStatus.SC_OK ) {
						webPage.setContent ( "" );
						cache.cache ( webPage );
						throw new WebApplicationException ( Response.Status.BAD_REQUEST );
					}
					InputStream is = response.getEntity ().getContent ();
					BufferedReader reader = new BufferedReader ( new InputStreamReader ( is ) );
					String content = "";
					String line;
					while ( ( line = reader.readLine () ) != null ) {
						content += line;
					}
					WebPage page = new WebPage ();
					page.setUrl ( webPage.getUrl () );
					page.setContent ( content );
					if ( cache.cache ( page ) ) {
						return page;
					} else {
						throw new CacheIOException ( "Unable to save!" );
					}
				} catch ( IOException e ) {
					webPage.setContent ( "" );
					cache.cache ( webPage );
					return webPage;
				}
			}
		}
	}

}
