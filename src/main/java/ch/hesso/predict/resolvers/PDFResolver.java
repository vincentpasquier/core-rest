/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resolvers;

import ch.hesso.cache.PDFCache;
import ch.hesso.cache.exceptions.CacheIOException;
import ch.hesso.cache.exceptions.CacheQueryException;
import ch.hesso.cache.query.CacheQuery;
import ch.hesso.predict.restful.PDFFile;
import com.google.common.io.BaseEncoding;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.HTTP;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PDFResolver extends GenericResolver {

	private final PDFCache cache;

	private static final String CONTENT_TYPE = "Content-Type";

	private static final String PDF_MEDIA_TYPE = com.google.common.net.MediaType.PDF.toString ();

	public PDFResolver () {
		PDFCache.Builder pdfBuilder = PDFCache.Builder.newBuilder ( new File ( "/mnt/Damocles/master/pdfs" ) );
		pdfBuilder.partitions ( 223 );
		cache = pdfBuilder.build ();
	}

	public PDFFile resolve ( final PDFFile pdfFile ) throws CacheQueryException, IOException, CacheIOException {
		CacheQuery<PDFFile> query = cache.newQuery ( pdfFile.getUrl () ).execute ();
		// Query returns result
		if ( query.hasMore () ) {
			return query.next ();
		} else { // No results
			HttpGet resolve = new HttpGet ( pdfFile.getUrl () );
			resolve.addHeader ( HTTP.USER_AGENT, USER_AGENTS.get ( RANDOM.nextInt ( USER_AGENTS.size () ) ) );
			HttpResponse response = WEB_CLIENT.execute ( resolve );
			Header contentType = response.getFirstHeader ( CONTENT_TYPE );
			boolean isPDF = contentType.getValue ().equals ( PDF_MEDIA_TYPE );
			if ( !isPDF ) {
				throw new WebApplicationException ( Response.Status.BAD_REQUEST );
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream ();
			response.getEntity ().writeTo ( baos );
			byte[] content = baos.toByteArray ();
			String encodedPDF = BaseEncoding.base64 ().encode ( content );
			pdfFile.setContent ( encodedPDF );

			if ( !cache.cache ( pdfFile ) ) {
				throw new CacheIOException ( "Unable to save PDF" );
			}

			return pdfFile;
		}
	}

}
