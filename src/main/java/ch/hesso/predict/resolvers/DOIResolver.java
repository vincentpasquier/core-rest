/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resolvers;

import ch.hesso.cache.DOICache;
import ch.hesso.cache.exceptions.CacheIOException;
import ch.hesso.cache.exceptions.CacheQueryException;
import ch.hesso.cache.query.CacheQuery;
import ch.hesso.predict.restful.Doi;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class DOIResolver {

	private final DOICache cache;

	private final HttpClient client;

	private static final String ACM_URL = "http://dl.acm.org/citation.cfm?id=";

	private static final String IEEEXPLORE_URL = "http://ieeexplore.ieee.org/xpl/articleDetails.jsp?tp=&arnumber=";

	private static final String SPRINGER_URL = "http://link.springer.com/chapter/10.1007/";

	private static final String ACM_DOI = "/10.1145/";

	private static final String IEEEXPLORE_DOI = "/10.1109/";

	private static final String SPRINGER_DOI = "/10.1007/";

	private static final Pattern PATTERN_ACM =
			Pattern.compile ( "10\\.1145/([0-9]*\\.[0-9]*)" );

	private static final Pattern PATTERN_IEEEXPLORE =
			Pattern.compile ( "10\\.1109/[A-z0-9\\-]*\\.[0-9]*\\.([0-9]*)" );

	private static final Pattern PATTERN_SPRINGER =
			Pattern.compile ( "10\\.1007/([0-9\\-_]{1,30})" );

	public DOIResolver () {
		DOICache.Builder doiBuilder = DOICache.Builder.newBuilder ( new File ( "/mnt/Damocles/master/doi" ) );
		doiBuilder.partitions ( 1 );
		cache = doiBuilder.build ();
		client = HttpClients.createMinimal ();
	}

	/**
	 * @param doi
	 *
	 * @return
	 *
	 * @throws IOException
	 */
	public Doi resolve ( final Doi doi ) throws IOException, CacheQueryException, CacheIOException {
		Doi potential = fastForward ( doi );
		if ( potential.getForward () != null ) {
			return potential;
		}
		CacheQuery<Doi> query = cache.newQuery ( doi.getDoi () ).execute ();
		if ( query.hasMore () ) {
			return query.next ();
		} else {
			HttpGet resolve = new HttpGet ( doi.getDoi () );
			HttpResponse response = client.execute ( resolve );
			int responseCode = response.getStatusLine ().getStatusCode ();
			if ( responseCode != HttpStatus.SC_SEE_OTHER ) {
				throw new MalformedURLException ();
			}
			Header location = response.getFirstHeader ( "location" );
			doi.setForward ( location.getValue () );
			if ( !cache.cache ( doi ) ) {
				throw new CacheIOException ( "Unable to save DOI" );
			}
		}
		return doi;
	}

	private Doi fastForward ( final Doi doi ) {
		Doi fastForwarded = new Doi ();
		if ( doi != null && doi.getDoi () != null && !doi.getDoi ().equals ( "" ) ) {
			Matcher matcher = null;
			if ( doi.getDoi ().contains ( SPRINGER_DOI ) ) {
				matcher = PATTERN_SPRINGER.matcher ( doi.getDoi () );
				if ( matcher.find () && matcher.group ( 1 ).length () > 5 ) {
					fastForwarded.setForward ( SPRINGER_URL + matcher.group ( 1 ) );
				}

			} else if ( doi.getDoi ().contains ( ACM_DOI ) ) {
				matcher = PATTERN_ACM.matcher ( doi.getDoi () );
				if ( matcher.find () && matcher.group ( 1 ).length () > 5 ) {
					fastForwarded.setForward ( ACM_URL + matcher.group ( 1 ) );
				}

			} else if ( doi.getDoi ().contains ( IEEEXPLORE_DOI ) ) {
				matcher = PATTERN_IEEEXPLORE.matcher ( doi.getDoi () );
				if ( matcher.find () && matcher.group ( 1 ).length () > 5 ) {
					fastForwarded.setForward ( IEEEXPLORE_URL + matcher.group ( 1 ) );
				}

			}
		}
		return fastForwarded;
	}
}
