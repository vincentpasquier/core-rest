/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resolvers;

import com.google.common.io.Files;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.Args;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public abstract class GenericResolver {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( GenericResolver.class );

	protected static final List<String> USER_AGENTS = new ArrayList<> ();

	protected static final PoolingHttpClientConnectionManager CONNECTION_MANAGER;

	protected static final HttpClient WEB_CLIENT;

	protected static final Random RANDOM = new Random ();

	static {
		try {
			USER_AGENTS.addAll ( Files.readLines (
					new File (
							GenericResolver.class.getResource ( "/crawler-agent.txt" ).getFile ()
					),
					Charset.defaultCharset () )
			);
		} catch ( IOException e ) {
			LOG.debug ( "Error while loading user agent file, check logs", e );
		}
		CONNECTION_MANAGER = new PoolingHttpClientConnectionManager ();
		CONNECTION_MANAGER.setMaxTotal ( 5000 );
		CONNECTION_MANAGER.setDefaultMaxPerRoute ( 5000 );

		RequestConfig config = RequestConfig.custom ()
				.setConnectTimeout ( 10 * 500 )
				.setConnectionRequestTimeout ( 10 * 500 )
				.setSocketTimeout ( 10 * 500 )
				.build ();

		WEB_CLIENT = HttpClients.custom ()
				.setDefaultRequestConfig ( config )
				.setRedirectStrategy ( new RedirectHttpGet () )
				.setConnectionManager ( CONNECTION_MANAGER )
				.build ();
	}

	/**
	 *
	 */
	private static final class RedirectHttpGet extends DefaultRedirectStrategy {

		@Override
		public boolean isRedirected (
				final HttpRequest request,
				final HttpResponse response,
				final HttpContext context ) throws ProtocolException {
			Args.notNull ( request, "HTTP request" );
			Args.notNull ( response, "HTTP response" );

			final int statusCode = response.getStatusLine ().getStatusCode ();
			final String method = request.getRequestLine ().getMethod ();
			final Header locationHeader = response.getFirstHeader ( "location" );
			switch ( statusCode ) {
				case HttpStatus.SC_MOVED_TEMPORARILY:
					return locationHeader != null;
				case HttpStatus.SC_MOVED_PERMANENTLY:
				case HttpStatus.SC_TEMPORARY_REDIRECT:
				case HttpStatus.SC_SEE_OTHER:
					return true;
				default:
					return false;
			} //end of switch
		}
	}
}
