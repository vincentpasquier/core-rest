/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.events.RESTEvents;
import ch.hesso.predict.events.api.APICreateEvent;
import ch.hesso.predict.repository.SesameRepository;
import ch.hesso.predict.restful.Conference;
import ch.hesso.predict.restful.Person;
import ch.hesso.predict.restful.Publication;
import ch.hesso.predict.restful.Venue;
import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.*;
import com.wordnik.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static ch.hesso.predict.repository.SesameRepository.LIMIT;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path ( Venue.RESOURCE_PATH )
@Api ( value = Venue.RESOURCE_PATH, description = "Venues operations" )
@Consumes ( MediaType.APPLICATION_JSON )
@Produces ( MediaType.APPLICATION_JSON )
public class VenueResource {

	@GET
	@ApiOperation ( value = "Finds all venues, uses pagination", response = Venue.class, responseContainer = "List" )
	public List<Venue> venues (
			@ApiParam ( value = "Pagination mechanism", required = false )
			@QueryParam ( value = "page" )
			final int page
	) {
		int offset = LIMIT * page;
		List<Venue> toReturn = new ArrayList<> ();
		SesameRepository.SesameResponse<VenueBean> venues =
				ServletLoading.VENUE_FACADE.repository ().getAllLimitOffset ( offset, "ven:venue", "" );

		for ( VenueBean venue : venues.getCollection () ) {
			Venue to = Transfer.VENUE_TRANSFER.fromStorage ( venue );
			toReturn.add ( to );
		}

		if ( toReturn.size () == LIMIT ) {
			toReturn.get ( LIMIT - 1 ).setHasMore ( true );
		}

		venues.close ();

		return toReturn;
	}

	@GET
	@Path ( "/{venueId}" )
	@ApiOperation ( value = "Find venue by ID", response = Venue.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid venue ID supplied" ),
			@ApiResponse ( code = 404, message = "Venue not found" )
	} )
	public Venue venue (
			@ApiParam ( value = "Venue ID to fetch", required = true )
			@PathParam ( "venueId" )
			final String venueId ) throws WebApplicationException {

		SesameRepository.SesameResponse<VenueBean> venue =
				ServletLoading.VENUE_FACADE.repository ().getById ( venueId, "" );
		if ( !venue.isValid () ) {
			venue.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Venue to = Transfer.VENUE_TRANSFER.fromStorage ( venue.getEntity () );
		venue.close ();

		return to;
	}

	/**
	 * @param keyId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/key/{keyId: .+}" )
	@ApiOperation ( value = "Get venue by key", response = Venue.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid venue key supplied" ),
			@ApiResponse ( code = 404, message = "Venue not found" )
	} )
	public Venue conferenceByKey (
			@ApiParam ( value = "Venue to fetch", required = true )
			@PathParam ( "keyId" )
			final String keyId ) throws WebApplicationException {

		SesameRepository.SesameResponse<VenueBean> venues =
				ServletLoading.VENUE_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " ven:key ?key",
						new String[] { keyId },
						new String[] { "key" },
						""
				);

		if ( venues.isEmpty () ) {
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		VenueBean venue = venues.first ();
		Venue to = Transfer.VENUE_TRANSFER.fromStorage ( venue );

		venues.close ();

		return to;
	}

	@GET
	@Path ( "/{venueId}/publications" )
	@ApiOperation ( value = "Get associated publications to venue by ID", response = Publication.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid venue ID supplied" ),
			@ApiResponse ( code = 404, message = "Venue not found" )
	} )
	public List<Publication> publicationsByVenue (
			@ApiParam ( value = "Venue ID to fetch", required = true )
			@PathParam ( "venueId" )
			final String venueId ) throws WebApplicationException {

		SesameRepository.SesameResponse<VenueBean> venue =
				ServletLoading.VENUE_FACADE.repository ().getById ( venueId, "" );
		if ( !venue.isValid () ) {
			venue.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Publication> publications = new ArrayList<> ();
		Set<PublicationBean> publicationsBean = venue.getEntity ().getPublications ();
		for ( PublicationBean publicationBean : publicationsBean ) {
			Publication to = Transfer.PUBLICATION_TRANSFER.fromStorage ( publicationBean );
			publications.add ( to );
		}

		venue.close ();

		return publications;
	}

	@GET
	@Path ( "/{venueId}/authors" )
	@ApiOperation ( value = "Get associated publications to venue by ID", response = Person.class, responseContainer = "Set" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid venue ID supplied" ),
			@ApiResponse ( code = 404, message = "Venue not found" )
	} )
	public Set<Person> authorsByVenue (
			@ApiParam ( value = "Venue ID to fetch", required = true )
			@PathParam ( "venueId" )
			final String venueId ) throws WebApplicationException {

		SesameRepository.SesameResponse<VenueBean> venue =
				ServletLoading.VENUE_FACADE.repository ().getById ( venueId, "" );
		if ( !venue.isValid () ) {
			venue.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Set<Person> people = new HashSet<> ();
		Set<PublicationBean> publicationsBean = venue.getEntity ().getPublications ();
		for ( PublicationBean publicationBean : publicationsBean ) {
			for ( PersonBean personBean : publicationBean.getAuthors () ) {
				Person to = Transfer.PERSON_TRANSFER.fromStorage ( personBean );
				people.add ( to );
			}
		}

		venue.close ();

		return people;
	}

	@GET
	@Path ( "/{venueId}/conference" )
	@ApiOperation ( value = "Get associated conference to venue by ID", response = Conference.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid venue ID supplied" ),
			@ApiResponse ( code = 404, message = "Venue not found" )
	} )
	public Conference conferencesByVenue (
			@ApiParam ( value = "Venue ID to fetch", required = true )
			@PathParam ( "venueId" )
			final String venueId ) throws WebApplicationException {

		SesameRepository.SesameResponse<VenueBean> venue =
				ServletLoading.VENUE_FACADE.repository ().getById ( venueId, "" );
		if ( !venue.isValid () ) {
			venue.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		ConferenceBean conferenceBean = venue.getEntity ().getConference ();
		if ( conferenceBean == null ) {
			venue.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Conference to = Transfer.CONFERENCE_TRANSFER.fromStorage ( conferenceBean );
		venue.close ();

		return to;
	}

	@POST
	@Consumes ( MediaType.APPLICATION_JSON )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Adds a venue" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "Venue added" ),
			@ApiResponse ( code = 400, message = "Similar Venue is already existing" )
	} )
	public Response add (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Venue to add", required = true )
			final Venue venue )
			throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		boolean valid = venue.getId () == null;
		valid &= venue.getKey () != null;
		valid &= venue.getYear () != 0;
		if ( !valid ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}

		SesameRepository.SesameResponse<VenueBean> beans =
				ServletLoading.VENUE_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " ven:key ?key",
						new String[] { venue.getKey () },
						new String[] { "key" },
						sessionId
				);

		if ( !beans.isEmpty () ) {
			VenueBean venueBean = beans.first ();
			String id = venueBean.getId ();
			beans.close ();
			return response.entity ( id ).build ();
		}

		venue.setSessionId ( sessionId );
		String id = IDStorage.generateId ( Venue.class, venue );
		venue.setId ( id );
		RESTEvents.BUS.publish ( new APICreateEvent<> ( venue ) );
		beans.close ();

		return response.entity ( id ).build ();
	}

	@POST
	@Path ( "/{venueId}/publication/{publicationId}" )
	@ApiOperation ( value = "Sets publication to venue by ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "Venue added" ),
			@ApiResponse ( code = 400, message = "Invalid publication ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "Publication not found" ),
			@ApiResponse ( code = 404, message = "Venue not found" ),
	} )
	public Response addVenueByPublication (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Venue ID to add", required = true )
			@PathParam ( "venueId" )
			final String venueId,
			@ApiParam ( value = "Publication ID to add venue to", required = true )
			@PathParam ( "publicationId" )
			final String publicationId ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check publicationId, conference
		SesameRepository.SesameResponse<VenueBean> venue =
				ServletLoading.VENUE_FACADE.repository ().getById ( venueId, sessionId );
		SesameRepository.SesameResponse<PublicationBean> publication =
				ServletLoading.PUBLICATION_FACADE.repository ().getById ( publicationId, sessionId );
		if ( !venue.isValid () || !publication.isValid () ) {
			venue.close ();
			publication.close ();
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}

		venue.getEntity ().addPublication ( publication.getEntity () );
		response.entity ( venue.getEntity ().getId () );

		venue.close ();
		publication.close ();

		return response.build ();
	}

	@POST
	@Path ( "/{venueId}/conference/{conferenceKey: .+}" )
	@ApiOperation ( value = "Sets Conference to venue by Key" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "Venue added" ),
			@ApiResponse ( code = 400, message = "Invalid Conference key supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" ),
			@ApiResponse ( code = 404, message = "Venue not found" ),
	} )
	public Response addVenueByConferenceKey (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Venue ID to add", required = true )
			@PathParam ( "venueId" )
			final String venueId,
			@ApiParam ( value = "Publication ID to add venue to", required = true )
			@PathParam ( "conferenceKey" )
			final String conferenceKey ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check publicationId, conference
		SesameRepository.SesameResponse<ConferenceBean> beans =
				ServletLoading.CONFERENCE_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " conf:key ?key",
						new String[] { conferenceKey },
						new String[] { "key" },
						sessionId
				);

		SesameRepository.SesameResponse<VenueBean> venue =
				ServletLoading.VENUE_FACADE.repository ().getById ( venueId, sessionId );
		if ( !venue.isEmpty () ) {
			ConferenceBean conferenceBean = beans.first ();
			if ( venue.isValid () && conferenceBean != null ) {
				venue.getEntity ().addConference ( conferenceBean );
				response.entity ( venue.getEntity ().getId () );
			} else {
				venue.close ();
				throw new WebApplicationException ( Response.Status.BAD_REQUEST );
			}
		}

		venue.close ();

		return response.build ();
	}
}
