/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.events.RESTEvents;
import ch.hesso.predict.events.api.APICreateEvent;
import ch.hesso.predict.events.api.APIModifyEvent;
import ch.hesso.predict.restful.Plugin;
import ch.hesso.predict.storage.Transfer;
import ch.hesso.websocket.entities.PluginBean;
import ch.hesso.websocket.server.registry.WebSocketPlugins;
import com.wordnik.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path (Plugin.RESOURCE_PATH)
@Api (value = Plugin.RESOURCE_PATH, description = "Plugins operations")
@Consumes (MediaType.APPLICATION_JSON)
@Produces (MediaType.APPLICATION_JSON)
public final class PluginsResource {

	@GET
	@ApiOperation (value = "Finds all plugins registered", response = Plugin.class, responseContainer = "List")
	public Collection<Plugin> plugins () {
		Collection<PluginBean> plugins = WebSocketPlugins.REGISTRY.plugins ();
		Collection<Plugin> toReturn = new ArrayList<> ( plugins.size () );
		for ( PluginBean bean : plugins ) {
			toReturn.add ( Transfer.PLUGIN_TRANSFER.fromStorage ( bean ) );
		}
		return toReturn;
	}

	@GET
	@Path ("/{pluginId}")
	@ApiOperation (value = "Find plugin by ID", response = Plugin.class)
	@ApiResponses (value = {
			@ApiResponse (code = 400, message = "Invalid Plugin ID supplied"),
			@ApiResponse (code = 404, message = "Plugin not found")
	})
	public Plugin pluginById ( @ApiParam (value = "PluginBean ID to fetch", required = true)
														 @PathParam ("pluginId")
														 final String pluginId ) throws WebApplicationException {
		PluginBean plugin = WebSocketPlugins.REGISTRY.plugin ( pluginId );
		if ( plugin == null ) {
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}
		return Transfer.PLUGIN_TRANSFER.fromStorage ( plugin );
	}

	@POST
	@Consumes (MediaType.APPLICATION_JSON)
	@Produces (MediaType.APPLICATION_JSON)
	@ApiOperation (value = "Registers a plugin")
	@ApiResponses (value = {
			@ApiResponse (code = 201, message = "Plugin registered"),
			@ApiResponse (code = 400, message = "Similar plugin is already registered")
	})
	public Response register (
			@ApiParam (value = "Plugin to register", required = true)
			final Plugin to
	) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		PluginBean plugin = Transfer.PLUGIN_TRANSFER.fromTransferObject ( to );
		boolean register = WebSocketPlugins.REGISTRY.register ( plugin );
		if ( !register ) {
			response = Response.status ( Response.Status.BAD_REQUEST );
			response.entity ( String.format ( "Unable to register plugin [%s]", plugin.getName () ) );
		} else {
			response.entity ( Transfer.PLUGIN_TRANSFER.fromStorage ( plugin ) );
			RESTEvents.BUS.publish ( new APICreateEvent<> ( to ) );
		}

		return response.build ();
	}

	@PUT
	@Path ("/{pluginId}")
	@ApiOperation (value = "Updates Plugin by ID")
	@ApiResponses (value = {
			@ApiResponse (code = 200, message = "Plugin updated"),
			@ApiResponse (code = 400, message = "Invalid Plugin ID supplied"),
			@ApiResponse (code = 404, message = "Plugin not found")
	})
	public Response update (
			@ApiParam (value = "Plugin ID to update", required = true)
			@PathParam ("pluginId")
			final String pluginId,
			@ApiParam (value = "Plugin to update", required = true)
			final Plugin plugin ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();
		PluginBean bean = WebSocketPlugins.REGISTRY.plugin ( pluginId );
		if ( bean == null ) {
			response = Response.status ( Response.Status.BAD_REQUEST );
		} else {
			PluginBean delta = Transfer.PLUGIN_TRANSFER.fromTransferObject ( plugin );
			WebSocketPlugins.REGISTRY.modify ( bean.getId (), delta );
			bean = WebSocketPlugins.REGISTRY.plugin ( pluginId );
			Plugin to = Transfer.PLUGIN_TRANSFER.fromStorage ( bean );
			RESTEvents.BUS.publish ( new APIModifyEvent<> ( to ) );
			response.entity ( to );
		}

		return response.build ();
	}

	@DELETE
	@Path ( "/{pluginId}" )
	@Consumes ( MediaType.APPLICATION_JSON )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Unregisters a plugin" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 200, message = "Plugin unregistered" ),
			@ApiResponse ( code = 404, message = "No plugin found" )
	} )
	public Response unregister (
			@ApiParam ( value = "Plugin to unregister", required = true )
			@PathParam ( "pluginId" )
			final String pluginId
	) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		PluginBean bean = WebSocketPlugins.REGISTRY.plugin ( pluginId );
		if ( bean == null ) {
			response = Response.status ( Response.Status.BAD_REQUEST );
		} else {
			WebSocketPlugins.REGISTRY.unregister ( bean );
		}

		// Check pluginId

		return response.build ();
	}

}
