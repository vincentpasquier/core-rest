/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.events.RESTEvents;
import ch.hesso.predict.events.api.APICreateEvent;
import ch.hesso.predict.repository.SesameRepository;
import ch.hesso.predict.restful.*;
import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.*;
import com.wordnik.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path ( Conference.RESOURCE_PATH )
@Api ( value = Conference.RESOURCE_PATH, description = "Conferences operations" )
@Consumes ( MediaType.APPLICATION_JSON )
@Produces ( MediaType.APPLICATION_JSON )
public final class ConferencesResource {

	/**
	 * @return
	 */
	@GET
	@ApiOperation ( value = "Finds all conferences, uses pagination", response = Conference.class, responseContainer = "List" )
	public List<Conference> conferences (
			@ApiParam ( value = "Pagination mechanism", required = false )
			@QueryParam ( value = "page" )
			final int page
	) {
		int offset = SesameRepository.LIMIT * page;
		List<Conference> toReturn = new ArrayList<> ();
		SesameRepository.SesameResponse<ConferenceBean> conferences =
				ServletLoading.CONFERENCE_FACADE
						.repository ()
						.getAllLimitOffset ( offset, "conf:conference", "" );

		if ( !conferences.isValid () ) {
			conferences.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		for ( ConferenceBean conferenceBean : conferences.getCollection () ) {
			Conference to = Transfer.CONFERENCE_TRANSFER.fromStorage ( conferenceBean );
			toReturn.add ( to );
		}

		if ( toReturn.size () == SesameRepository.LIMIT ) {
			toReturn.get ( SesameRepository.LIMIT - 1 ).setHasMore ( true );
		}

		conferences.close ();

		return toReturn;
	}

	/**
	 * @param conferenceId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{conferenceId}" )
	@ApiOperation ( value = "Finds conference by ID", response = Conference.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" )
	} )
	public Conference conferenceById (
			@ApiParam ( value = "Conference ID to fetch", required = true )
			@PathParam ( "conferenceId" )
			final String conferenceId ) throws WebApplicationException {

		SesameRepository.SesameResponse<ConferenceBean> conference =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, "" );
		if ( !conference.isValid () ) {
			conference.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Conference to = Transfer.CONFERENCE_TRANSFER.fromStorage ( conference.getEntity () );
		conference.close ();

		return to;
	}

	/**
	 * @param conferenceId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{conferenceId}/committees" )
	@ApiOperation ( value = "Get associated program committee to a Conference ID", response = Person.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" )
	} )
	public List<Person> programCommitteeByConference (
			@ApiParam ( value = "Conference ID to fetch", required = true )
			@PathParam ( "conferenceId" )
			final String conferenceId ) throws WebApplicationException {

		SesameRepository.SesameResponse<ConferenceBean> conference =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, "" );
		if ( !conference.isValid () ) {
			conference.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Person> people = new ArrayList<> ();
		for ( PersonBean peopleBean : conference.getEntity ().getPcMembers () ) {
			people.add ( Transfer.PERSON_TRANSFER.fromStorage ( peopleBean ) );
		}
		conference.close ();

		return people;
	}

	/**
	 * @param conferenceId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{conferenceId}/authors" )
	@ApiOperation ( value = "Get associated authors to a Conference ID", response = Person.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" )
	} )
	public List<Person> authorsByConference (
			@ApiParam ( value = "Conference ID to fetch", required = true )
			@PathParam ( "conferenceId" )
			final String conferenceId ) throws WebApplicationException {

		SesameRepository.SesameResponse<ConferenceBean> conference =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, "" );
		if ( !conference.isValid () ) {
			conference.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Person> people = new ArrayList<> ();
		for ( VenueBean venue : conference.getEntity ().getVenues () ) {
			for ( PublicationBean publications : venue.getPublications () ) {
				for ( PersonBean author : publications.getAuthors () ) {
					people.add ( Transfer.PERSON_TRANSFER.fromStorage ( author ) );
				}
			}
		}
		conference.close ();

		return people;
	}

	/**
	 * @param conferenceId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{conferenceId}/publications" )
	@ApiOperation ( value = "Get associated publications to conference by ID", response = Publication.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" )
	} )
	public List<Publication> publicationsByConference (
			@ApiParam ( value = "Conference ID to fetch", required = true )
			@PathParam ( "conferenceId" )
			final String conferenceId ) throws WebApplicationException {

		SesameRepository.SesameResponse<ConferenceBean> conference =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, "" );
		if ( !conference.isValid () ) {
			conference.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Publication> publications = new ArrayList<> ();
		for ( VenueBean venue : conference.getEntity ().getVenues () ) {
			for ( PublicationBean publication : venue.getPublications () ) {
				publications.add ( Transfer.PUBLICATION_TRANSFER.fromStorage ( publication ) );
			}
		}
		conference.close ();

		return publications;
	}

	/**
	 * @param conferenceId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{conferenceId}/venues" )
	@ApiOperation ( value = "Get associated venues to conference by ID", response = Publication.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" )
	} )
	public List<Venue> venuesByConference (
			@ApiParam ( value = "Conference ID to fetch", required = true )
			@PathParam ( "conferenceId" )
			final String conferenceId ) throws WebApplicationException {

		SesameRepository.SesameResponse<ConferenceBean> conference =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, "" );
		if ( !conference.isValid () ) {
			conference.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Venue> venues = new ArrayList<> ();
		for ( VenueBean venue : conference.getEntity ().getVenues () ) {
			venues.add ( Transfer.VENUE_TRANSFER.fromStorage ( venue ) );
		}
		return venues;
	}

	/**
	 * @param conferenceId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{conferenceId}/calls" )
	@ApiOperation ( value = "Get associated calls to conference by ID", response = Publication.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" )
	} )
	public List<Cfp> callsByConference (
			@ApiParam ( value = "Conference ID to fetch", required = true )
			@PathParam ( "conferenceId" )
			final String conferenceId ) throws WebApplicationException {

		SesameRepository.SesameResponse<ConferenceBean> conference =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, "" );
		if ( !conference.isValid () ) {
			conference.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Cfp> cfps = new ArrayList<> ();
		for ( CfpBean cfp : conference.getEntity ().getCfp () ) {
			cfps.add ( Transfer.CFP_TRANSFER.fromStorage ( cfp ) );
		}
		conference.close ();

		return cfps;
	}

	/**
	 * @param keyId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/key/{keyId: .+}" )
	@ApiOperation ( value = "Get conference by key", response = Conference.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference key supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" )
	} )
	public Conference conferenceByKey (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Conference to fetch", required = true )
			@PathParam ( "keyId" )
			final String keyId ) throws WebApplicationException {

		SesameRepository.SesameResponse<ConferenceBean> conferences =
				ServletLoading.CONFERENCE_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " conf:key ?key",
						new String[] { keyId },
						new String[] { "key" },
						sessionId
				);

		if ( conferences.isValid () ) {
			conferences.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		ConferenceBean conference = conferences.first ();
		Conference conf = Transfer.CONFERENCE_TRANSFER.fromStorage ( conference );
		conferences.close ();

		return conf;
	}

	/**
	 * @param conference
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Consumes ( MediaType.APPLICATION_JSON )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Adds a conference" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "ConferenceBean added" ),
			@ApiResponse ( code = 400, message = "Similar conference is already existing" )
	} )
	public Response add (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "ConferenceBean to add", required = true )
			final Conference conference )
			throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check ConferenceBean
		boolean valid = conference.getId () == null;
		valid &= conference.getKey () != null;
		valid &= conference.getAcronym () != null;
		valid &= conference.getTitle () != null;
		if ( !valid ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}
		SesameRepository.SesameResponse<ConferenceBean> conferences =
				ServletLoading.CONFERENCE_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " conf:key ?key",
						new String[] { conference.getKey () },
						new String[] { "key" }, ""
				);

		if ( !conferences.isEmpty () ) {
			ConferenceBean conferenceBean = conferences.first ();
			String id = conferenceBean.getId ();
			conferences.close ();

			return response.entity ( id ).build ();
		}
		conference.setSessionId ( sessionId );
		String id = IDStorage.generateId ( Conference.class, conference );
		conference.setId ( id );

		RESTEvents.BUS.publish ( new APICreateEvent<> ( conference ) );
		conferences.close ();

		return response.entity ( id ).build ();
	}

	/**
	 * @param conferenceId
	 * @param venueId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Path ( "/{conferenceId}/venue/{venueId}" )
	@ApiOperation ( value = "Adds venue to conference by ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid venue ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" ),
			@ApiResponse ( code = 404, message = "Publication not found" ),
	} )
	public Response addPublicationByConference (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Conference ID to add venue to", required = true )
			@PathParam ( "conferenceId" )
			final String conferenceId,
			@ApiParam ( value = "Venue ID to add", required = true )
			@PathParam ( "venueId" )
			final String venueId ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check conferenceId, publicationId
		SesameRepository.SesameResponse<ConferenceBean> conf =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, sessionId );
		SesameRepository.SesameResponse<VenueBean> venue =
				ServletLoading.VENUE_FACADE.repository ().getById ( venueId, sessionId );
		if ( !conf.isValid () || !venue.isValid () ) {
			conf.close ();
			venue.close ();
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}

		venue.getEntity ().addConference ( conf.getEntity () );
		response.entity ( conf.getEntity ().getId () );

		conf.close ();
		venue.close ();

		return response.build ();
	}

	/**
	 * @param conferenceId
	 * @param personId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Path ( "/{conferenceId}/people/{personId}" )
	@ApiOperation ( value = "Adds person to conference by ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 404, message = "ConferenceBean not found" ),
			@ApiResponse ( code = 404, message = "PersonBean not found" ),
	} )
	public Response addPeopleByConference (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "ConferenceBean ID to add person to", required = true )
			@PathParam ( "conferenceId" )
			final String conferenceId,
			@ApiParam ( value = "PersonBean ID to add", required = true )
			@PathParam ( "personId" )
			final String personId ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check conferenceId, check people
		SesameRepository.SesameResponse<ConferenceBean> conf =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, sessionId );
		SesameRepository.SesameResponse<PersonBean> person =
				ServletLoading.PERSON_FACADE.repository ().getById ( personId, sessionId );
		if ( !conf.isValid () || !person.isValid () ) {
			conf.close ();
			person.close ();
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}

		person.getEntity ().addConference ( conf.getEntity () );
		response.entity ( conf.getEntity ().getId () );

		conf.close ();
		person.close ();

		return response.build ();
	}

	/**
	 * @param sessionId
	 * @param ranking
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Path ( "/ranking" )
	@ApiOperation ( value = "Adds ranking to conference by ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" ),
	} )
	public Response addRanking (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Ranking to add", required = true )
			final Ranking ranking ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		SesameRepository.SesameResponse<ConferenceBean> conferences =
				ServletLoading.CONFERENCE_FACADE.repository ().getByField (
						"{ " + SesameRepository.ENTITY + " conf:title ?title }"
								+ " UNION { \n"
								+ SesameRepository.ENTITY + " conf:acronym ?acr }",
						new String[] { ranking.getTitle (),
								ranking.getAcronym () },
						new String[] { "title", "acr" }, sessionId
				);

		if ( !conferences.isEmpty () ) {
			ConferenceBean bean = conferences.first ();
			bean.setHindex ( ranking.getFieldRating () );
			bean.setPublicationCount ( ranking.getPublication () );
			bean.addCategory ( ranking.getCategory () );
		}
		conferences.close ();

		return response.build ();
	}

	/**
	 * @param sessionId
	 * @param cfp
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Path ( "/cfp" )
	@ApiOperation ( value = "Adds cfp to conference by ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 404, message = "Conference not found" ),
	} )
	public Response addCfp (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Cfp to add", required = true )
			final Cfp cfp ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		SesameRepository.SesameResponse<ConferenceBean> conferences =
				ServletLoading.CONFERENCE_FACADE.repository ().getByField (
						"{ " + SesameRepository.ENTITY + " conf:title ?title }"
								+ " UNION { \n"
								+ SesameRepository.ENTITY + " conf:acronym ?acr }",
						new String[] { cfp.getTitle (),
								cfp.getAcronym ().toLowerCase () },
						new String[] { "title", "acr" }, sessionId
				);

		if ( !conferences.isEmpty () ) {
			SesameRepository.SesameResponse<CfpBean> cfpBean =
					ServletLoading.CFP_FACADE.repository ().getById ( cfp.getId (), sessionId );
			if ( cfpBean.isValid () ) {
				ConferenceBean bean = conferences.first ();
				cfpBean.getEntity ().addConference ( bean );
				bean.addCategories ( cfp.getKeywords () );
				response.entity ( cfp.getId () );
			}
			cfpBean.close ();
		}
		conferences.close ();

		return response.build ();
	}

}
