/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.IDStorage;
import com.wordnik.swagger.annotations.*;
import org.apache.commons.httpclient.HttpStatus;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path ( "/sessions" )
@Api ( value = "/sessions", description = "Session operations" )
@Consumes ( MediaType.APPLICATION_JSON )
@Produces ( MediaType.APPLICATION_JSON )
public class BulkOperationsResource {

	public BulkOperationsResource() {

	}

	@GET
	@ApiOperation ( value = "Starts a bulk add session" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 200, message = "Plugin updated" ),
			@ApiResponse ( code = 400, message = "Invalid Plugin ID supplied" ),
			@ApiResponse ( code = 404, message = "Plugin not found" )
	} )
	public Response startSession () {
		Response.ResponseBuilder response = Response.ok ();
		try {
			ObjectConnection connection = ServletLoading.SESSION_FACADE.repository ().connection ();
			String sessionId = IDStorage.generateId ( String.class, "session" );
			ServletLoading.CONNECTIONS.putIfAbsent ( sessionId, connection );
			connection.begin ();
			response.entity ( sessionId );
		} catch ( RepositoryException e ) {
			e.printStackTrace ();
		}
		return response.build ();
	}

	@POST
	@Path ( "/{session}" )
	@ApiOperation ( value = "Commit a session by its ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 200, message = "Plugin updated" ),
			@ApiResponse ( code = 400, message = "Invalid Plugin ID supplied" ),
			@ApiResponse ( code = 404, message = "Plugin not found" )
	} )
	public Response commitSession (
			@PathParam ( "session" )
			@ApiParam ( value = "Session to commit", required = true )
			final String sessionId
	) {
		Response.ResponseBuilder response = Response.ok ();
		if ( sessionId != null && ServletLoading.CONNECTIONS.containsKey ( sessionId ) ) {
			ObjectConnection connection = ServletLoading.CONNECTIONS.get ( sessionId );
			ServletLoading.CONNECTIONS.remove ( sessionId, connection );
			try {
				connection.commit ();
				response.entity ( sessionId );
			} catch ( RepositoryException e ) {
				response.entity ( HttpStatus.SC_INTERNAL_SERVER_ERROR );
			}
		} else {
			response.status ( HttpStatus.SC_BAD_REQUEST );
		}
		return response.build ();
	}

	@DELETE
	@Path ( "/{sessionId}" )
	@ApiOperation ( value = "Rollback a session by its ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 200, message = "Plugin updated" ),
			@ApiResponse ( code = 400, message = "Invalid Plugin ID supplied" ),
			@ApiResponse ( code = 404, message = "Plugin not found" )
	} )
	public Response rollback (
			@PathParam ( "sessionId" )
			@ApiParam ( value = "Session to rollback", required = true )
			final String sessionId
	) {
		Response.ResponseBuilder response = Response.ok ();
		if ( ServletLoading.CONNECTIONS.containsKey ( sessionId ) ) {
			ObjectConnection connection = ServletLoading.CONNECTIONS.get ( sessionId );
			ServletLoading.CONNECTIONS.remove ( sessionId, connection );
			try {
				connection.rollback ();
				response.entity ( sessionId );
			} catch ( RepositoryException e ) {
				response.entity ( HttpStatus.SC_INTERNAL_SERVER_ERROR );
			}
		} else {
			response.status ( HttpStatus.SC_BAD_REQUEST );
		}
		return response.build ();
	}
}
