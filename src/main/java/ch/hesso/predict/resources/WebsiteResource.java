/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.resolvers.PDFResolver;
import ch.hesso.predict.resolvers.WebResolver;
import ch.hesso.predict.restful.PDFFile;
import ch.hesso.predict.restful.WebPage;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path (WebPage.RESOURCE_PATH)
@Api (value = WebPage.RESOURCE_PATH, description = "Webpage and PDF extraction")
@Consumes (MediaType.APPLICATION_JSON)
@Produces (MediaType.APPLICATION_JSON)
public class WebsiteResource {

	private static final WebResolver WEB_RESOLVER = new WebResolver ();

	private static final PDFResolver PDF_RESOLVER = new PDFResolver ();

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( WebsiteResource.class );

	/**
	 * @return
	 */
	@POST
	@ApiOperation ( value = "Get a Webpage content", response = WebPage.class, responseContainer = "List" )
	public WebPage webPage (
			@ApiParam ( value = "WebPage", required = false )
			final WebPage webPage
	) {
		if ( webPage == null || webPage.getUrl () == null ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}
		try {
			return WEB_RESOLVER.page ( webPage );
		} catch ( Exception e ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}
	}

	/**
	 * @return
	 */
	@POST
	@Path ( "/pdf" )
	@ApiOperation ( value = "Get a PDF file content", response = WebPage.class, responseContainer = "List" )
	public PDFFile pdf (
			@ApiParam ( value = "WebPage", required = false )
			final PDFFile pdfFile
	) {
		if ( pdfFile == null || pdfFile.getUrl () == null ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}
		try {
			return PDF_RESOLVER.resolve ( pdfFile );
		} catch ( Exception e ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}
	}

}
