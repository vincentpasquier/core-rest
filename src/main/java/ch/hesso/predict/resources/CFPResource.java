/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.events.RESTEvents;
import ch.hesso.predict.events.api.APICreateEvent;
import ch.hesso.predict.repository.SesameRepository;
import ch.hesso.predict.restful.Cfp;
import ch.hesso.predict.restful.Conference;
import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.CfpBean;
import ch.hesso.predict.storage.ConferenceBean;
import ch.hesso.predict.storage.IDStorage;
import ch.hesso.predict.storage.Transfer;
import com.wordnik.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static ch.hesso.predict.repository.SesameRepository.LIMIT;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path ( Cfp.RESOURCE_PATH )
@Api ( value = Cfp.RESOURCE_PATH, description = "Call for papers operations" )
@Produces ( MediaType.APPLICATION_JSON )
public final class CFPResource {

	private static final int CFP_LIMIT = 200;

	/**
	 * @return
	 */
	@GET
	@ApiOperation ( value = "Finds all call for papers, uses pagination", response = Cfp.class, responseContainer = "List" )
	public List<Cfp> cfp (
			@ApiParam ( value = "Pagination mechanism", required = false )
			@QueryParam ( value = "page" )
			final int page
	) {
		int offset = CFP_LIMIT * page;
		List<Cfp> toReturn = new ArrayList<> ();
		SesameRepository.SesameResponse<CfpBean> cfps =
				ServletLoading.CFP_FACADE.repository ().getAllLimitOffset ( offset, CFP_LIMIT, "cfp:cfp", "" );

		if ( !cfps.isValid () ) {
			return toReturn;
		}

		for ( CfpBean cfpBean : cfps.getCollection () ) {
			Cfp cfp = new Cfp ();
			cfp.setId ( cfpBean.getId () );
			cfp.setDeadlines ( cfpBean.getDeadlines () );
			cfp.setReferrers ( cfpBean.getReferrers () );
			cfp.setKeywords ( cfpBean.getKeywords () );
			cfp.setRelated ( cfpBean.getRelated () );
			cfp.setOrigin ( cfpBean.getOrigin () );
			cfp.setWebsite ( cfpBean.getWebsite () );
			cfp.setTitle ( cfpBean.getTitle () );
			cfp.setAcronym ( cfpBean.getAcronym () );
			cfp.setStart ( cfpBean.getStart () );
			cfp.setEnd ( cfpBean.getEnd () );
			cfp.setLocation ( cfpBean.getLocation () );
			toReturn.add ( cfp );
		}

		if ( toReturn.size () == CFP_LIMIT ) {
			toReturn.get ( CFP_LIMIT - 1 ).setHasMore ( true );
		}

		cfps.close ();

		return toReturn;
	}

	/**
	 * @param cfpId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{cfpId}" )
	@ApiOperation ( value = "Finds call for paper by ID", response = Cfp.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid CfpBean ID supplied" ),
			@ApiResponse ( code = 404, message = "CfpBean not found" )
	} )
	public Cfp cfpById (
			@ApiParam ( value = "CfpBean ID to fetch", required = true )
			@PathParam ( "cfpId" )
			final String cfpId ) throws WebApplicationException {

		SesameRepository.SesameResponse<CfpBean> cfp =
				ServletLoading.CFP_FACADE.repository ().getById ( cfpId, "" );
		if ( !cfp.isValid () ) {
			cfp.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Cfp to = Transfer.CFP_TRANSFER.fromStorage ( cfp.getEntity () );
		cfp.close ();

		return to;
	}

	/**
	 * @param cfpId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{cfpId}/conference" )
	@ApiOperation ( value = "Gets associated conference by ID", response = Conference.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid CfpBean ID supplied" ),
			@ApiResponse ( code = 404, message = "CfpBean not found" )
	} )
	public Conference conferenceByCFP (
			@ApiParam ( value = "CfpBean ID to fetch", required = true )
			@PathParam ( "cfpId" )
			final String cfpId ) throws WebApplicationException {

		SesameRepository.SesameResponse<CfpBean> cfp =
				ServletLoading.CFP_FACADE.repository ().getById ( cfpId, "" );
		if ( !cfp.isValid () ) {
			cfp.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		if ( cfp.getEntity ().getConference () == null ) {
			cfp.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Conference to = Transfer.CONFERENCE_TRANSFER.fromStorage ( cfp.getEntity ().getConference () );
		cfp.close ();

		return to;
	}

	/**
	 * @param cfp
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Consumes ( MediaType.APPLICATION_JSON )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Adds a call for paper", position = 3 )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "CfpBean added" ),
			@ApiResponse ( code = 400, message = "Similar CfpBean is already existing" )
	} )
	public Response add (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "CfpBean to add", required = true )
			final Cfp cfp )
			throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check CfpBean object
		boolean valid = cfp.getId () == null;
		valid &= cfp.getOrigin () != null && cfp.getOrigin ().length () > 0;

		if ( !valid ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}
		// Check whether it already exists or not
		SesameRepository.SesameResponse<CfpBean> beans = ServletLoading.CFP_FACADE.repository ().getByField (
				SesameRepository.ENTITY + " cfp:origin ?origin",
				new String[] { cfp.getOrigin () },
				new String[] { "origin" },
				sessionId
		);

		if ( beans.isValid () ) {
			CfpBean cfpBean = beans.first ();
			beans.close ();
			return response.entity ( cfpBean.getId () ).build ();
		}

		cfp.setSessionId ( sessionId );
		String id = IDStorage.generateId ( Cfp.class, cfp );
		cfp.setId ( id );

		RESTEvents.BUS.publish ( new APICreateEvent<> ( cfp ) );
		beans.close ();

		return response.entity ( id ).build ();
	}

	/**
	 * @param cfpId
	 * @param conferenceId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Path ( "/{cfpId}/conference/{conferenceId}" )
	@ApiOperation ( value = "Sets associated conference by ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid Cfp ID supplied" ),
			@ApiResponse ( code = 404, message = "Cfp not found" )
	} )
	public Response addConference (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Cfp ID to fetch", required = true )
			@PathParam ( "cfpId" )
			final String cfpId,
			@ApiParam ( value = "Conference to set for Cfp" )
			@PathParam ( "conferenceId" )
			final String conferenceId
	) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check cfpId exists, conferenceId exists
		SesameRepository.SesameResponse<CfpBean> cfp =
				ServletLoading.CFP_FACADE.repository ().getById ( cfpId, sessionId );

		SesameRepository.SesameResponse<ConferenceBean> conference =
				ServletLoading.CONFERENCE_FACADE.repository ().getById ( conferenceId, sessionId );

		if ( cfp.isValid () && conference.isValid () ) {
			cfp.getEntity ().addConference ( conference.getEntity () );
		} else {
			cfp.close ();
			conference.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		cfp.close ();
		conference.close ();

		return response.entity ( cfpId ).build ();
	}
}
