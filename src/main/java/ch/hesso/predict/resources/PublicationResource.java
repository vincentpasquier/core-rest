/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.events.RESTEvents;
import ch.hesso.predict.events.api.APICreateEvent;
import ch.hesso.predict.repository.SesameRepository;
import ch.hesso.predict.restful.Conference;
import ch.hesso.predict.restful.Person;
import ch.hesso.predict.restful.Publication;
import ch.hesso.predict.restful.Venue;
import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.*;
import com.wordnik.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static ch.hesso.predict.repository.SesameRepository.LIMIT;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path ( Publication.RESOURCE_PATH )
@Api ( value = Publication.RESOURCE_PATH, description = "Publications operations" )
@Consumes ( MediaType.APPLICATION_JSON )
@Produces ( MediaType.APPLICATION_JSON )
public final class PublicationResource {

	@GET
	@ApiOperation ( value = "Finds all publications, uses pagination", response = Publication.class, responseContainer = "List" )
	public List<Publication> publications (
			@ApiParam ( value = "Pagination mechanism", required = false )
			@QueryParam ( value = "page" )
			final int page
	) {
		int offset = LIMIT * page;
		List<Publication> toReturn = new ArrayList<> ();
		SesameRepository.SesameResponse<PublicationBean> publications =
				ServletLoading.PUBLICATION_FACADE.repository ().getAllLimitOffset (
						offset, "pub:publication", "" );

		for ( PublicationBean publication : publications.getCollection () ) {
			Publication to = Transfer.PUBLICATION_TRANSFER.fromStorage ( publication );
			toReturn.add ( to );
		}

		if ( toReturn.size () == LIMIT ) {
			toReturn.get ( LIMIT - 1 ).setHasMore ( true );
		}

		publications.close ();

		return toReturn;
	}

	@GET
	@Path ( "/{publicationId}" )
	@ApiOperation ( value = "Finds publication by ID", response = Publication.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid publication ID supplied" ),
			@ApiResponse ( code = 404, message = "Publication not found" )
	} )
	public Publication publicationById (
			@ApiParam ( value = "Publication ID to fetch", required = true )
			@PathParam ( "publicationId" )
			final String publicationId ) throws WebApplicationException {

		SesameRepository.SesameResponse<PublicationBean> publication =
				ServletLoading.PUBLICATION_FACADE.repository ().getById ( publicationId, "" );
		if ( !publication.isValid () ) {
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Publication to = Transfer.PUBLICATION_TRANSFER.fromStorage ( publication.getEntity () );
		publication.close ();

		return to;
	}


	@GET
	@Path ( "/key/{publicationKey: .+}" )
	@ApiOperation ( value = "Finds publication by key", response = Publication.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid publication key supplied" ),
			@ApiResponse ( code = 404, message = "Publication not found" )
	} )
	public Publication publicationByKey (
			@ApiParam ( value = "Publication key to fetch", required = true )
			@PathParam ( "publicationKey" )
			final String publicationKey ) throws WebApplicationException {

		SesameRepository.SesameResponse<PublicationBean> publications =
				ServletLoading.PUBLICATION_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " pub:key ?key",
						new String[] { publicationKey },
						new String[] { "key" },
						""
				);
		if ( publications.isEmpty () ) {
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		PublicationBean publication = publications.first ();

		Publication to = Transfer.PUBLICATION_TRANSFER.fromStorage ( publication );

		publications.close ();

		return to;
	}

	@GET
	@Path ( "/{publicationId}/venue" )
	@ApiOperation ( value = "Get associated venue to publication by ID", response = Conference.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid publication ID supplied" ),
			@ApiResponse ( code = 404, message = "Publication not found" )
	} )
	public Venue venueByPublication (
			@ApiParam ( value = "Publication ID to fetch", required = true )
			@PathParam ( "publicationId" )
			final String publicationId ) throws WebApplicationException {

		SesameRepository.SesameResponse<PublicationBean> publication =
				ServletLoading.PUBLICATION_FACADE.repository ().getById ( publicationId, "" );
		if ( !publication.isValid () ) {
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		VenueBean venue = publication.getEntity ().getVenue ();
		if ( venue == null ) {
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Venue to = Transfer.VENUE_TRANSFER.fromStorage ( venue );

		publication.close ();

		return to;
	}

	@GET
	@Path ( "/{publicationId}/authors" )
	@ApiOperation ( value = "Get associated people to publication by ID", response = Person.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid publication ID supplied" ),
			@ApiResponse ( code = 404, message = "Publication not found" )
	} )
	public List<Person> peopleByPublication (
			@ApiParam ( value = "Publication ID to fetch", required = true )
			@PathParam ( "publicationId" )
			final String publicationId ) throws WebApplicationException {

		SesameRepository.SesameResponse<PublicationBean> publication =
				ServletLoading.PUBLICATION_FACADE.repository ().getById ( publicationId, "" );
		if ( !publication.isValid () ) {
			publication.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Set<PersonBean> authorsBean = publication.getEntity ().getAuthors ();
		if ( authorsBean == null || authorsBean.isEmpty () ) {
			publication.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Person> authors = new ArrayList<> ();
		for ( PersonBean author : authorsBean ) {
			authors.add ( Transfer.PERSON_TRANSFER.fromStorage ( author ) );
		}

		publication.close ();

		return authors;
	}

	@POST
	@Consumes ( MediaType.APPLICATION_JSON )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Adds a publication" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "Publication added" ),
			@ApiResponse ( code = 400, message = "Similar publication is already existing" )
	} )
	public Response add (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Publication to add", required = true )
			final Publication publication )
			throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		boolean valid = publication.getId () == null;
		valid &= publication.getKey () != null;
		valid &= publication.getTitle () != null;
		if ( !valid ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}

		SesameRepository.SesameResponse<PublicationBean> beans =
				ServletLoading.PUBLICATION_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " pub:key ?key",
						new String[] { publication.getKey () },
						new String[] { "key" },
						sessionId
				);

		if ( !beans.isEmpty () ) {
			PublicationBean publicationBean = beans.first ();
			String id = publicationBean.getId ();
			beans.close ();
			return response.entity ( id ).build ();
		}

		publication.setSessionId ( sessionId );
		String id = IDStorage.generateId ( Publication.class, publication );
		publication.setId ( id );
		RESTEvents.BUS.publish ( new APICreateEvent<> ( publication ) );

		beans.close ();

		return response.entity ( publication.getId () ).build ();
	}

	@POST
	@Path ( "/{publicationId}/venue/{venueKey: .+}" )
	@ApiOperation ( value = "Sets venue to publication by Key" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "Venue added" ),
			@ApiResponse ( code = 400, message = "Invalid publication ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "Publication not found" ),
			@ApiResponse ( code = 404, message = "Venue not found" ),
	} )
	public Response addVenueByPublication (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Publication ID to add venue to", required = true )
			@PathParam ( "publicationId" )
			final String publicationId,
			@ApiParam ( value = "Venue ID to add", required = true )
			@PathParam ( "venueKey" )
			final String venueKey ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check publicationId, conference
		SesameRepository.SesameResponse<VenueBean> beans =
				ServletLoading.VENUE_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " ven:key ?key",
						new String[] { venueKey },
						new String[] { "key" },
						sessionId
				);
		SesameRepository.SesameResponse<PublicationBean> publication =
				ServletLoading.PUBLICATION_FACADE.repository ().getById ( publicationId, sessionId );

		if ( !beans.isEmpty () ) {
			VenueBean venueBean = beans.first ();
			if ( publication != null && venueBean != null ) {
				publication.getEntity ().addVenue ( venueBean );
				response.entity ( publication.getEntity ().getId () );
			} else {
				beans.close ();
				publication.close ();
				throw new WebApplicationException ( Response.Status.BAD_REQUEST );
			}
		}

		beans.close ();
		publication.close ();

		return response.build ();
	}

	@POST
	@Path ( "/{publicationId}/author/{authorId}" )
	@ApiOperation ( value = "Adds author to publication by ID" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "Person added" ),
			@ApiResponse ( code = 400, message = "Invalid publication ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 404, message = "Publication not found" ),
			@ApiResponse ( code = 404, message = "Person not found" ),
	} )
	public Response addPersonByPublication (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "Publication ID to add author to", required = true )
			@PathParam ( "publicationId" )
			final String publicationId,
			@ApiParam ( value = "Person ID to add", required = true )
			@PathParam ( "authorId" )
			final String authorId ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		SesameRepository.SesameResponse<PersonBean> person =
				ServletLoading.PERSON_FACADE.repository ().getById ( authorId, sessionId );
		SesameRepository.SesameResponse<PublicationBean> publication =
				ServletLoading.PUBLICATION_FACADE.repository ().getById ( publicationId, sessionId );
		if ( !person.isValid () || !publication.isValid () ) {
			person.close ();
			publication.close ();
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}

		publication.getEntity ().addAuthor ( person.getEntity (), true );
		response.entity ( Transfer.PUBLICATION_TRANSFER.fromStorage ( publication.getEntity () ) );

		person.close ();
		publication.close ();

		return response.build ();
	}

	@PUT
	@Path ( "/{publicationId}" )
	@ApiOperation ( value = "Modify publication" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "Publication modified" ),
			@ApiResponse ( code = 400, message = "Invalid publication ID supplied" ),
			@ApiResponse ( code = 404, message = "Publication not found" ),
	} )
	public Response modifyPublication (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "", required = true )
			@PathParam ( "publicationId" )
			final String publicationId,
			@ApiParam ( value = "Publication to modify", required = true )
			final Publication publication
	) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		SesameRepository.SesameResponse<PublicationBean> publicationBean =
				ServletLoading.PUBLICATION_FACADE.repository ().getById ( publicationId, sessionId );
		if ( publicationBean.isValid () ) {
			publicationBean.getEntity ().setContent ( publication.getContent () );
			publicationBean.getEntity ().setUrl ( publication.getUrl () );
		} else {
			response = Response.status ( Response.Status.BAD_REQUEST );
		}

		publicationBean.close ();

		return response.build ();
	}

}
