/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.resolvers.DOIResolver;
import ch.hesso.predict.restful.Doi;
import com.wordnik.swagger.annotations.*;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path (Doi.RESOURCE_PATH)
@Api (value = Doi.RESOURCE_PATH, description = "Digital Object Identifier operations")
@Produces (MediaType.APPLICATION_JSON)
public final class DOIResource {

	private static final DOIResolver RESOLVER = new DOIResolver ();

	/**
	 * @param doi
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@ApiOperation (value = "Finds DOI by ID", response = Doi.class)
	@ApiResponses (value = {
			@ApiResponse (code = 400, message = "Invalid DOI supplied"),
			@ApiResponse (code = 404, message = "DOI not found")
	})
	public Doi doiById (
			@ApiParam ( value = "DOI to fetch", required = true )
			final Doi doi
	) throws WebApplicationException {

		try {
			return RESOLVER.resolve ( doi );
		} catch ( final Exception e ) {
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}
	}

}
