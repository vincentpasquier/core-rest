/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.repository.SesameRepository;
import ch.hesso.predict.restful.Person;
import ch.hesso.predict.restful.PublicationAbstract;
import ch.hesso.predict.restful.Recommendation;
import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.*;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.ObjectQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path (Recommendation.RESOURCE_PATH)
@Api (value = Recommendation.RESOURCE_PATH, description = "Recommendation operations")
@Consumes (MediaType.APPLICATION_JSON)
@Produces (MediaType.APPLICATION_JSON)
public class RecommendationResource {

	private static final int TYPEAHEAD_LIMIT = 8;

	private static final Set<String> acronyms = new TreeSet<> ();

	private static final Set<String> keywords = new TreeSet<> ();

	private static final Set<String> titles = new TreeSet<> ();

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( RecommendationResource.class );

	static {
		SesameRepository.SesameResponse<ConferenceBean> response =
				ServletLoading.CONFERENCE_FACADE.repository ().getAll ( "" );
		if ( response.isValid () ) {
			for ( ConferenceBean conferenceBean : response.getCollection () ) {
				for ( String keyword : conferenceBean.getKeywords () ) {
					keywords.add ( keyword );
				}
				acronyms.add ( conferenceBean.getAcronym ().toUpperCase () );
				titles.add ( conferenceBean.getTitle () );
			}
		}
		response.close ();
	}

	/**
	 * @param query
	 *
	 * @return
	 */
	@GET
	@Path ( "/keywords" )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Get keywords for typeahead", response = String.class, responseContainer = "List" )
	public List<String> keywords (
			@ApiParam ( value = "Keywords to fetch", required = true )
			@QueryParam ( "query" ) String query
	) {
		return queryTypeAhead ( keywords, query );
	}

	/**
	 * @param query
	 *
	 * @return
	 */
	@GET
	@Path ( "/acronyms" )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Get acronyms for typeahead", response = String.class, responseContainer = "List" )
	public List<String> acronyms (
			@ApiParam ( value = "Keywords to fetch", required = true )
			@QueryParam ( "query" ) String query
	) {
		return queryTypeAhead ( acronyms, query );
	}

	/**
	 * @param query
	 *
	 * @return
	 */
	@GET
	@Path ("/titles")
	@Produces (MediaType.APPLICATION_JSON)
	@ApiOperation (value = "Get titles for typeahead", response = String.class, responseContainer = "List")
	public List<String> titles (
			@ApiParam (value = "Keywords to fetch", required = true)
			@QueryParam ("query") String query
	) {
		return queryTypeAhead ( titles, query );
	}

	/**
	 * @param recommendation
	 *
	 * @return
	 */
	@POST
	@Produces (MediaType.APPLICATION_JSON)
	@ApiOperation (value = "Get abstracts for all publications enclosed in recommendation parameters.", response = PublicationAbstract.class, responseContainer = "List")
	public List<PublicationAbstract> abstracts (
			@ApiParam (value = "Recommendation to get publications abstracts from", required = true)
			final Recommendation recommendation
	) {

		Set<String> conferenceBeans = new HashSet<> ();

		if ( !recommendation.getConferenceTitle ().isEmpty () || !recommendation.getConferenceAcronym ().isEmpty () ) {
			// OR
			LOG.info ( "Conference" );
			conferenceBeans.addAll ( conferenceFilter (
					recommendation.getConferenceTitle (), "conf:title" ) );

			conferenceBeans.addAll ( conferenceFilter (
					recommendation.getConferenceAcronym (), "conf:acronym" ) );
		} else if ( !recommendation.getKeywords ().isEmpty () ) {
			// AND
			LOG.info ( "Keywords" );
			conferenceBeans.addAll ( conferenceFilterKeywords (
					recommendation.getKeywords (), "conf:keywords" ) );
		}

		Set<String> venuesBeans = venueFilter ( conferenceBeans,
				recommendation.getStartYear (),
				recommendation.getEndYear () );

		List<PublicationAbstract> toReturn = extractAbstracts ( venuesBeans );

		// Get all possible venues [startYear, endYear]
		// Get all publication within these venues
		// Extract abstract and create PublicationAbstract

		return toReturn;
	}

	/**
	 * @param recommendation
	 *
	 * @return
	 */
	@POST
	@Path ( "/committee" )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Get program committee for all publications enclosed in recommendation parameters.", response = Person.class, responseContainer = "List" )
	public List<Person> committee (
			@ApiParam (value = "Recommendation to get committee from", required = true)
			final Recommendation recommendation
	) {

		List<Person> toReturn = new ArrayList<> ();

		Map<String, Integer> peopleId = new HashMap<> ();

		Set<String> conferenceBeans = new HashSet<> ();

		if ( recommendation.getConferenceTitle ().size () > 0 || recommendation.getConferenceAcronym ().size () > 0 ) {
			// OR
			conferenceBeans.addAll ( conferenceFilter (
					recommendation.getConferenceTitle (), "conf:title" ) );

			conferenceBeans.addAll ( conferenceFilter (
					recommendation.getConferenceAcronym (), "conf:acronym" ) );
		} else if ( recommendation.getKeywords ().size () > 0 ) {
			// AND
			conferenceBeans.addAll ( conferenceFilterKeywords (
					recommendation.getKeywords (), "conf:keywords" ) );
		}

		for ( String conference : conferenceBeans ) {
			SesameRepository.SesameResponse<ConferenceBean> response =
					ServletLoading.CONFERENCE_FACADE.repository ().getById ( conference, "" );
			if ( response.isValid () ) {
				ConferenceBean conferenceBean = response.getEntity ();
				for ( PersonBean personBean : conferenceBean.getPcMembers () ) {
					String id = personBean.getId ();
					if ( !peopleId.containsKey ( id ) ) {
						peopleId.put ( id, conferenceBean.getHindex () );
					} else {
						peopleId.put ( id, peopleId.get ( id ) + conferenceBean.getHindex () );
					}
				}
			}
			response.close ();
		}

		for ( Map.Entry<String, Integer> entries : peopleId.entrySet () ) {
			SesameRepository.SesameResponse<PersonBean> response =
					ServletLoading.PERSON_FACADE.repository ().getById ( entries.getKey (), "" );
			if ( response.isValid () ) {
				Person to = Transfer.PERSON_TRANSFER.fromStorage ( response.getEntity () );
				to.setRanking ( entries.getValue () );
				toReturn.add ( to );
			}
			response.close ();
		}

		Collections.sort ( toReturn );

		// Get all person associated with conferences
		// Get all publication within these venues
		// Extract abstract and create PublicationAbstract

		return toReturn;
	}

	/**
	 * @param toSearch
	 * @param iri
	 *
	 * @return
	 */
	private Set<String> conferenceFilter (
			final Set<String> toSearch,
			final String iri
	) {
		Set<String> conferenceBeans = new HashSet<> ();
		try {
			ObjectConnection connection = ServletLoading.CONFERENCE_FACADE.repository ().connection ();
			for ( String keyword : toSearch ) {
				ObjectQuery query = connection.prepareObjectQuery ( QueryLanguage.SPARQL,
						StorageEntity.SHORT_NS +
								"SELECT ?entity WHERE { ?entity " + iri + " ?search }"
				);
				query.setBinding ( "search", connection.getValueFactory ().createLiteral ( keyword ) );
				for ( ConferenceBean conferenceBean : query.evaluate ( ConferenceBean.class ).asList () ) {
					conferenceBeans.add ( conferenceBean.getId () );
				}
			}
			connection.close ();
		} catch ( RepositoryException | MalformedQueryException | QueryEvaluationException e ) {
			LOG.error ( "Error while executing conference filter, check logs.", e );
		}

		return conferenceBeans;
	}

	/**
	 * @param toSearch
	 * @param iri
	 *
	 * @return
	 */
	private Collection<? extends String> conferenceFilterKeywords (
			final Set<String> toSearch,
			final String iri
	) {
		Multimap<String, String> conferencesKeywords = HashMultimap.create ();
		try {
			ObjectConnection connection = ServletLoading.CONFERENCE_FACADE.repository ().connection ();
			for ( String keyword : toSearch ) {
				ObjectQuery query = connection.prepareObjectQuery ( QueryLanguage.SPARQL,
						StorageEntity.SHORT_NS +
								"SELECT ?entity WHERE { ?entity " + iri + " ?search }"
				);
				query.setBinding ( "search", connection.getValueFactory ().createLiteral ( keyword ) );
				for ( ConferenceBean conferenceBean : query.evaluate ( ConferenceBean.class ).asList () ) {
					conferencesKeywords.put ( conferenceBean.getId (), keyword );
				}
			}
			connection.close ();
		} catch ( RepositoryException | MalformedQueryException | QueryEvaluationException e ) {
			LOG.error ( "Error while executing conference filter, check logs.", e );
		}

		int keywordCount = toSearch.size ();
		Set<String> conferenceBeans = new HashSet<> ();
		for ( String confId : conferencesKeywords.keySet () ) {
			boolean hasKeywords = conferencesKeywords.get ( confId ).size () == keywordCount;
			if ( hasKeywords ) {
				conferenceBeans.add ( confId );
			}
		}

		LOG.info ( String.format ( "Filtered %d conferences to %d.", conferencesKeywords.keySet ().size (), conferenceBeans.size () ) );

		return conferenceBeans;
	}

	/**
	 * @param conferenceBeans
	 * @param startYear
	 * @param endYear
	 *
	 * @return
	 */
	private Set<String> venueFilter ( final Set<String> conferenceBeans,
																		final int startYear,
																		final int endYear ) {
		Set<String> venues = new HashSet<> ();

		for ( String conference : conferenceBeans ) {
			SesameRepository.SesameResponse<ConferenceBean> response =
					ServletLoading.CONFERENCE_FACADE.repository ().getById ( conference, "" );
			if ( response.isValid () ) {
				ConferenceBean bean = response.getEntity ();
				for ( VenueBean venueBean : bean.getVenues () ) {
					int year = venueBean.getYear ();
					if ( year >= startYear && year <= endYear ) {
						venues.add ( venueBean.getId () );
					}
				}
			}
			response.close ();
		}

		return venues;
	}

	/**
	 * @param venuesBeans
	 *
	 * @return
	 */
	private List<PublicationAbstract> extractAbstracts ( final Set<String> venuesBeans ) {
		List<PublicationAbstract> publicationAbstracts = new ArrayList<> ();

		for ( String venueId : venuesBeans ) {
			SesameRepository.SesameResponse<VenueBean> response =
					ServletLoading.VENUE_FACADE.repository ().getById ( venueId, "" );
			if ( response.isValid () ) {
				VenueBean venueBean = response.getEntity ();
				for ( PublicationBean publicationBean : venueBean.getPublications () ) {
					PublicationAbstract pAbstract = new PublicationAbstract ();
					pAbstract.setYear ( publicationBean.getYear () );
					pAbstract.setText ( publicationBean.getTitle () );
					publicationAbstracts.add ( pAbstract );
				}
			}
			response.close ();
		}

		return publicationAbstracts;
	}

	//
	private List<String> queryTypeAhead ( final Set<String> data, final String match ) {
		List<String> toReturn = new ArrayList<> ( TYPEAHEAD_LIMIT );
		int count = 0;
		String lMatch = match != null ? match.toLowerCase () : "";
		for ( String key : data ) {
			if ( match == null || match.equals ( "" ) || key.toLowerCase ().contains ( lMatch ) ) {
				toReturn.add ( key );
				count++;
				if ( count > TYPEAHEAD_LIMIT ) {
					break;
				}
			}
		}
		return toReturn;
	}

}
