/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.resources;

import ch.hesso.predict.events.RESTEvents;
import ch.hesso.predict.events.api.APICreateEvent;
import ch.hesso.predict.repository.SesameRepository;
import ch.hesso.predict.restful.Conference;
import ch.hesso.predict.restful.Person;
import ch.hesso.predict.restful.Publication;
import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.*;
import com.wordnik.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static ch.hesso.predict.repository.SesameRepository.LIMIT;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Path ( Person.RESOURCE_PATH )
@Api ( value = Person.RESOURCE_PATH, description = "People operations" )
@Consumes ( MediaType.APPLICATION_JSON )
@Produces ( MediaType.APPLICATION_JSON )
public final class PeopleResource {

	/**
	 * @return
	 */
	@GET
	@ApiOperation ( value = "Finds all people, uses pagination", response = Person.class, responseContainer = "List" )
	public List<Person> people (
			@ApiParam ( value = "Pagination mechanism", required = false )
			@QueryParam ( value = "page" )
			final int page
	) throws WebApplicationException {
		int offset = LIMIT * page;
		List<Person> toReturn = new ArrayList<> ();

		SesameRepository.SesameResponse<PersonBean> people =
				ServletLoading.PERSON_FACADE.repository ().getAllLimitOffset ( offset, "per:person", "" );
		if ( !people.isValid () ) {
			people.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		for ( PersonBean person : people.getCollection () ) {
			Person to = Transfer.PERSON_TRANSFER.fromStorage ( person );
			toReturn.add ( to );
		}

		if ( toReturn.size () == LIMIT ) {
			toReturn.get ( LIMIT - 1 ).setHasMore ( true );
		}

		people.close ();

		return toReturn;
	}

	/**
	 * @param personId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{personId}" )
	@ApiOperation ( value = "Finds person by ID", response = Person.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 404, message = "PersonBean not found" )
	} )
	public Person personById (
			@ApiParam ( value = "PersonBean ID to fetch", required = true )
			@PathParam ( "personId" )
			final String personId
	) throws WebApplicationException {

		SesameRepository.SesameResponse<PersonBean> person
				= ServletLoading.PERSON_FACADE.repository ().getById ( personId, "" );
		if ( !person.isValid () ) {
			person.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		Person to = Transfer.PERSON_TRANSFER.fromStorage ( person.getEntity () );
		person.close ();

		return to;
	}

	/**
	 * @param name
	 *
	 * @return
	 */
	@GET
	@Path ( "/name/{nameStr: .+}" )
	@Consumes ( MediaType.APPLICATION_JSON )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Searches people based on criterion", response = Person.class )
	@ApiResponses ( value = {
			@ApiResponse ( code = 200, message = "People found" ),
			@ApiResponse ( code = 404, message = "No one found" )
	} )
	public Person search (
			@ApiParam ( value = "Search criterion", required = true )
			@PathParam ( value = "nameStr" )
			final String name
	) {

		SesameRepository.SesameResponse<PersonBean> beans =
				ServletLoading.PERSON_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " per:name ?name",
						new String[] { name },
						new String[] { "name" },
						""
				);

		if ( beans.isEmpty () ) {
			beans.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		PersonBean bean = beans.first ();
		Person to = Transfer.PERSON_TRANSFER.fromStorage ( bean );
		beans.close ();

		return to;
	}

	/**
	 * @param personId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{personId}/conferences" )
	@ApiOperation ( value = "Get associated conference to a person by ID", response = Conference.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 404, message = "PersonBean not found" )
	} )
	public List<Conference> conferencesByPerson (
			@ApiParam ( value = "PersonBean ID to fetch", required = true )
			@PathParam ( "personId" )
			final String personId ) throws WebApplicationException {

		SesameRepository.SesameResponse<PersonBean> person =
				ServletLoading.PERSON_FACADE.repository ().getById ( personId, "" );
		if ( !person.isValid () ) {
			person.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Conference> conferences = new ArrayList<> ();
		for ( ConferenceBean conferenceBean : person.getEntity ().getConferences () ) {
			Conference conf = Transfer.CONFERENCE_TRANSFER.fromStorage ( conferenceBean );
			conferences.add ( conf );
		}

		person.close ();

		return conferences;
	}

	/**
	 * @param personId
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@GET
	@Path ( "/{personId}/publications" )
	@ApiOperation ( value = "Get associated publications to a person by ID", response = Publication.class, responseContainer = "List" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 404, message = "PersonBean not found" )
	} )
	public List<Publication> publicationsByPerson (
			@ApiParam ( value = "PersonBean ID to fetch", required = true )
			@PathParam ( "personId" )
			final String personId ) throws WebApplicationException {

		SesameRepository.SesameResponse<PersonBean> person =
				ServletLoading.PERSON_FACADE.repository ().getById ( personId, "" );
		if ( !person.isValid () ) {
			person.close ();
			throw new WebApplicationException ( Response.Status.NOT_FOUND );
		}

		List<Publication> publications = new ArrayList<> ();
		for ( PublicationBean publicationBean : person.getEntity ().getPublications () ) {
			Publication publication = Transfer.PUBLICATION_TRANSFER.fromStorage ( publicationBean );
			publications.add ( publication );
		}

		person.close ();

		return publications;
	}

	/**
	 * @param person
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Consumes ( MediaType.APPLICATION_JSON )
	@Produces ( MediaType.APPLICATION_JSON )
	@ApiOperation ( value = "Adds a person" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 201, message = "PersonBean added" ),
			@ApiResponse ( code = 400, message = "Similar person is already existing" )
	} )
	public Response add (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "PersonBean to add", required = true )
			final Person person )
			throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check PersonBean
		boolean valid = person.getId () == null;
		valid &= person.getName () != null;
		valid &= person.getName ().length () > 0;

		if ( !valid ) {
			throw new WebApplicationException ( Response.Status.BAD_REQUEST );
		}
		SesameRepository.SesameResponse<PersonBean> beans =
				ServletLoading.PERSON_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " per:name ?name",
						new String[] { person.getName () },
						new String[] { "name" },
						sessionId
				);

		if ( !beans.isEmpty () ) {
			PersonBean personBean = beans.first ();
			String id = personBean.getId ();
			beans.close ();
			return response.entity ( id ).build ();
		}

		person.setSessionId ( sessionId );
		String id = IDStorage.generateId ( Person.class, person );
		person.setId ( id );
		RESTEvents.BUS.publish ( new APICreateEvent<> ( person ) );
		beans.close ();

		return response.entity ( id ).build ();
	}

	/**
	 * @param personId
	 * @param conferenceKey
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Path ( "/{personId}/conference/{conferenceKey: .+}" )
	@ApiOperation ( value = "Adds conference to person by Key" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid conference ID supplied" ),
			@ApiResponse ( code = 404, message = "PersonBean not found" ),
			@ApiResponse ( code = 404, message = "ConferenceBean not found" ),
	} )
	public Response addConferenceByPerson (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "PersonBean ID to add conference to", required = true )
			@PathParam ( "personId" )
			final String personId,
			@ApiParam ( value = "ConferenceBean ID to add", required = true )
			@PathParam ( "conferenceKey" )
			final String conferenceKey ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check personId, conferenceId
		SesameRepository.SesameResponse<PersonBean> person =
				ServletLoading.PERSON_FACADE.repository ().getById ( personId, sessionId );

		SesameRepository.SesameResponse<ConferenceBean> beans =
				ServletLoading.CONFERENCE_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " conf:key ?key",
						new String[] { conferenceKey },
						new String[] { "key" },
						sessionId );

		if ( !beans.isEmpty () ) {
			ConferenceBean conferenceBean = beans.first ();
			if ( conferenceBean != null && person.isValid () ) {
				person.getEntity ().addConference ( conferenceBean );
				response.entity ( person.getEntity ().getId () );
			} else {
				person.close ();
				beans.close ();
				throw new WebApplicationException ( Response.Status.BAD_REQUEST );
			}
		}
		person.close ();
		beans.close ();

		return response.build ();
	}

	/**
	 * @param personId
	 * @param publicationKey
	 *
	 * @return
	 *
	 * @throws WebApplicationException
	 */
	@POST
	@Path ( "/{personId}/publication/{publicationKey: .+}" )
	@ApiOperation ( value = "Adds publication to person by Key" )
	@ApiResponses ( value = {
			@ApiResponse ( code = 400, message = "Invalid person ID supplied" ),
			@ApiResponse ( code = 400, message = "Invalid publication ID supplied" ),
			@ApiResponse ( code = 404, message = "PersonBean not found" ),
			@ApiResponse ( code = 404, message = "PublicationBean not found" ),
	} )
	public Response addPublicationByPerson (
			@ApiParam ( value = "Session to add to", required = false )
			@QueryParam ( "sessionId" )
			final String sessionId,
			@ApiParam ( value = "PersonBean ID to add publication to", required = true )
			@PathParam ( "personId" )
			final String personId,
			@ApiParam ( value = "PublicationBean ID to add", required = true )
			@PathParam ( "publicationKey" )
			final String publicationKey ) throws WebApplicationException {

		Response.ResponseBuilder response = Response.ok ();

		// Check personId, publicationId
		SesameRepository.SesameResponse<PersonBean> person =
				ServletLoading.PERSON_FACADE.repository ().getById ( personId, sessionId );

		SesameRepository.SesameResponse<PublicationBean> beans =
				ServletLoading.PUBLICATION_FACADE.repository ().getByField (
						SesameRepository.ENTITY + " pub:key ?key",
						new String[] { publicationKey },
						new String[] { "key" },
						sessionId
				);

		if ( !beans.isEmpty () ) {
			PublicationBean publication = beans.first ();
			if ( publication != null && person.isValid () ) {
				person.getEntity ().addPublication ( publication, false );
				response.entity ( publication.getId () );
			} else {
				person.close ();
				beans.close ();
				throw new WebApplicationException ( Response.Status.BAD_REQUEST );
			}
		}

		person.close ();
		beans.close ();

		return response.build ();
	}

}
