/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.repository;

import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.nativerdf.NativeStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class RepositoryFactory<T> {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( RepositoryFactory.class );

	private final SesameRepository<T> _repository;

	private static final Repository store;

	static {
		String storePath = System.getProperty ( "predict.core.datastore" );
		if ( storePath == null ) {
			LOG.info ( "Datastore property not set (predict.core.datastore). Using default folder." );
			store = new SailRepository ( new NativeStore ( new File ( "datastore" ) ) );
		} else {
			store = new SailRepository ( new NativeStore ( new File ( storePath ) ) );
		}
		try {
			store.initialize ();
		} catch ( RepositoryException e ) {
			LOG.error ( "Sail repository connection failed, check logs.", e );
		}
	}

	public static void stop () {
		try {
			store.shutDown ();
		} catch ( RepositoryException e ) {
		}
	}

	private RepositoryFactory ( final Class<T> $class ) {
		_repository = new SesameRepository<> ( $class, store );
	}

	public static <T> RepositoryFactory<T> newFactory ( final Class<T> $class ) {
		return new RepositoryFactory<> ( $class );
	}

	public SesameRepository<T> repository () {
		return _repository;
	}

}
