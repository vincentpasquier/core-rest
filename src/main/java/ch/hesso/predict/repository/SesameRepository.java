/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.repository;

import ch.hesso.predict.servlet.ServletLoading;
import ch.hesso.predict.storage.StorageEntity;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.config.RepositoryConfigException;
import org.openrdf.repository.object.ObjectConnection;
import org.openrdf.repository.object.ObjectQuery;
import org.openrdf.repository.object.ObjectRepository;
import org.openrdf.repository.object.config.ObjectRepositoryFactory;
import org.openrdf.result.MultipleResultException;
import org.openrdf.result.NoResultException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class SesameRepository<T> {

	public static final int LIMIT = 100;

	public static final String ENTITY = "?entity";

	private final String GET_BY_ID = StorageEntity.SHORT_NS +
			"SELECT " + ENTITY + " WHERE { " + ENTITY + " ent:id ?id }\n" +
			"LIMIT 1";

	private final String GET_LIMIT_OFFSET = StorageEntity.SHORT_NS +
			"SELECT " + ENTITY + " WHERE { " + ENTITY + " ?x $id$ }\n" +
			"LIMIT $limit$\n" +
			"OFFSET ";

	private final String GET_BY_FIELD = StorageEntity.SHORT_NS +
			"SELECT " + ENTITY + " WHERE { $query$ }";

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( SesameRepository.class );

	private final Class<T> _$class;

	private ObjectRepository _repository;

	protected SesameRepository ( final Class<T> $class, final Repository store ) {
		_$class = $class;
		final Repository _store = store;
		final ObjectRepositoryFactory _factory = new ObjectRepositoryFactory ();
		try {
			_repository = _factory.createRepository ( _store );
		} catch ( RepositoryConfigException | RepositoryException e ) {
			LOG.error ( "Error while configuring repository, check logs.", e );
		}
	}

	/**
	 * @param sessionId
	 *
	 * @return
	 */
	public SesameResponse<T> getAll ( final String sessionId ) {
		Set<T> entities = new HashSet<> ();
		ObjectConnection connection = connection ( sessionId );
		try {
			if ( connection != null ) {
				entities = connection.getObjects ( _$class ).asSet ();
			}
		} catch ( RepositoryException | QueryEvaluationException e ) {
			LOG.debug ( String.format ( "Query error, does not return any object of type %s", _$class.getSimpleName () ), e );
		}
		return new SesameResponse<> ( entities, null, connection, sessionId );
	}

	/**
	 * @param offset
	 * @param object
	 * @param sessionId
	 *
	 * @return
	 */
	public SesameResponse<T> getAllLimitOffset ( final int offset, final String object, final String sessionId ) {
		return getAllLimitOffset ( offset, LIMIT, object, sessionId );
	}

	/**
	 * @param offset
	 * @param object
	 * @param sessionId
	 *
	 * @return
	 */
	public SesameResponse<T> getAllLimitOffset ( final int offset, final int limit, final String object, final String sessionId ) {
		Set<T> entities = new HashSet<> ();
		ObjectConnection connection = connection ( sessionId );
		try {
			String queryString = GET_LIMIT_OFFSET + offset;
			queryString = queryString.replace ( "$id$", object );
			queryString = queryString.replace ( "$limit$", String.valueOf ( limit ) );
			ObjectQuery query = connection.prepareObjectQuery ( queryString );
			entities = query.evaluate ( _$class ).asSet ();
		} catch ( MalformedQueryException | RepositoryException e ) {
			LOG.debug ( String.format ( "Query is malformed [%s], check logs", GET_LIMIT_OFFSET ), e );
		} catch ( QueryEvaluationException e ) {
			LOG.debug ( String.format ( "Query evaluation failed [%s], check logs", GET_LIMIT_OFFSET ), e );
		}
		return new SesameResponse<> ( entities, null, connection, sessionId );
	}


	/**
	 * @param id
	 * @param sessionId
	 *
	 * @return
	 */
	public SesameResponse<T> getById ( final String id, final String sessionId ) {
		T entity = null;
		ObjectConnection connection = connection ( sessionId );
		try {
			ObjectQuery query = connection.prepareObjectQuery ( GET_BY_ID );
			query.setObject ( "id", id );
			entity = query.evaluate ( _$class ).singleResult ();
		} catch ( NoResultException | MultipleResultException e ) {
			LOG.debug ( String.format ( "Query returns zero or more than one result [%s]", GET_BY_ID ), e );
		} catch ( MalformedQueryException | RepositoryException e ) {
			LOG.debug ( String.format ( "Query is malformed [%s], check logs", GET_BY_ID ), e );
		} catch ( QueryEvaluationException e ) {
			LOG.debug ( String.format ( "Query evaluation failed [%s], check logs", GET_BY_ID ), e );
		}
		return new SesameResponse<> ( null, entity, connection, sessionId );
	}

	/**
	 * @param queryString
	 * @param object
	 * @param fields
	 * @param sessionId
	 *
	 * @return
	 */
	public SesameResponse<T> getByField ( final String queryString, final Object[] object, final String[] fields, final String sessionId ) {
		Set<T> entities = new HashSet<> ();
		if ( object.length != fields.length ) {
			return new SesameResponse<> ( entities, null, null, sessionId );
		}
		ObjectConnection connection = connection ( sessionId );
		try {
			ObjectQuery query =
					connection.prepareObjectQuery ( GET_BY_FIELD.replace ( "$query$", queryString ) );
			for ( int i = 0; i < object.length; i++ ) {
				query.setObject ( fields[ i ], object[ i ] );
			}
			entities = query.evaluate ( _$class ).asSet ();
		} catch ( RepositoryException | MalformedQueryException e ) {
			LOG.debug ( String.format ( "Query is malformed [%s], check logs", GET_BY_FIELD ), e );
		} catch ( QueryEvaluationException e ) {
			LOG.debug ( String.format ( "Query evaluation failed [%s], check logs", GET_BY_FIELD ), e );
		}
		return new SesameResponse<> ( entities, null, connection, sessionId );
	}

	/**
	 * @param sessionId
	 *
	 * @return
	 *
	 * @throws RepositoryException
	 */
	public final ObjectConnection connection ( final String sessionId ) {
		if ( sessionId != null && ServletLoading.CONNECTIONS.containsKey ( sessionId ) ) {
			return ServletLoading.CONNECTIONS.get ( sessionId );
		} else {
			try {
				return _repository.getConnection ();
			} catch ( RepositoryException e ) {
				return null;
			}
		}
	}

	public final ObjectConnection connection () throws RepositoryException {
		return _repository.getConnection ();
	}

	public static final class SesameResponse<T> {

		private static final ScheduledExecutorService executor =
				Executors.newScheduledThreadPool ( 1 );

		private Collection<T> collection;

		private T entity;

		private ObjectConnection connection;

		private String sessionId;

		public SesameResponse (
				final Collection<T> collection,
				final T entity,
				final ObjectConnection connection,
				final String sessionId ) {
			this.collection = collection;
			this.entity = entity;
			this.connection = connection;
			this.sessionId = sessionId;
		}

		public Collection<T> getCollection () {
			return collection;
		}

		public boolean isValid () {
			return connection != null &&
					( ( collection != null &&
							!collection.isEmpty () )
							|| ( entity != null ) );
		}

		public T first () {
			return collection.iterator ().next ();
		}

		public T getEntity () {
			return entity;
		}

		public ObjectConnection getConnection () {
			return connection;
		}

		public void close () {
			executor.schedule ( new Runnable () {
				@Override
				public void run () {
					try {
						if ( sessionId.equals ( "" ) && connection != null && connection.isOpen () ) {
							connection.close ();
						}
					} catch ( RepositoryException e ) {
						e.printStackTrace ();
					}
				}
			}, 20, TimeUnit.SECONDS );
		}

		public boolean isEmpty () {
			return collection != null
					&& collection.isEmpty ();
		}
	}
}
