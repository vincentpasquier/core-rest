/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.storage;

import ch.hesso.predict.restful.*;
import ch.hesso.websocket.entities.PluginBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;

/**
 * @param <T>
 * @param <V>
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public interface Transfer<T, V> {

	/**
	 * @param storage
	 *
	 * @return
	 */
	V fromStorage ( final T storage );

	/**
	 * @param to
	 *
	 * @return
	 */
	T fromTransferObject ( final V to );

	public static final Transfer<CfpBean, Cfp> CFP_TRANSFER
			= new Transfer<CfpBean, Cfp> () {

		// SLF4J Logger
		private final Logger LOG = LoggerFactory.getLogger ( Transfer.class );

		@Override
		public Cfp fromStorage ( final CfpBean storage ) {
			Cfp cfp = new Cfp ();
			cfp.setId ( storage.getId () );
			cfp.setContent ( storage.getContent () );
			cfp.setDeadlines ( storage.getDeadlines () );
			cfp.setReferrers ( storage.getReferrers () );
			cfp.setKeywords ( storage.getKeywords () );
			cfp.setRelated ( storage.getRelated () );
			cfp.setOrigin ( storage.getOrigin () );
			cfp.setWebsite ( storage.getWebsite () );
			cfp.setTitle ( storage.getTitle () );
			cfp.setAcronym ( storage.getAcronym () );
			cfp.setStart ( storage.getStart () );
			cfp.setEnd ( storage.getEnd () );
			cfp.setLocation ( storage.getLocation () );
			return cfp;
		}

		@Override
		public CfpBean fromTransferObject ( final Cfp to ) {
			CfpBean cfp = new CfpBean ();
			cfp.setId ( to.getId () );
			cfp.setWebsite ( to.getWebsite () );
			cfp.setOrigin ( to.getOrigin () );
			cfp.setContent ( to.getContent () );
			cfp.setDeadlines ( to.getDeadlines () );
			cfp.setKeywords ( to.getKeywords () );
			cfp.setReferrers ( to.getReferrers () );
			cfp.setRelated ( to.getRelated () );
			cfp.setTitle ( to.getTitle () );
			cfp.setAcronym ( to.getAcronym () );
			cfp.setStart ( to.getStart () );
			cfp.setEnd ( to.getEnd () );
			cfp.setLocation ( to.getLocation () );
			return cfp;
		}
	};

	public static final Transfer<PersonBean, Person> PERSON_TRANSFER
			= new Transfer<PersonBean, Person> () {
		@Override
		public Person fromStorage ( final PersonBean storage ) {
			Person person = new Person ();
			person.setId ( storage.getId () );
			person.setName ( storage.getName () );
			person.setTitle ( storage.getTitle () );
			person.setRank ( storage.getRank () );
			person.setCountry ( storage.getCountry () );
			person.setEstablishments ( storage.getEstablishments () );
			return person;
		}

		@Override
		public PersonBean fromTransferObject ( final Person to ) {
			PersonBean person = new PersonBean ();
			person.setId ( to.getId () );
			person.setName ( to.getName () );
			person.setTitle ( to.getTitle () );
			person.setRank ( to.getRank () );
			person.setCountry ( to.getCountry () );
			person.setEstablishments ( new HashSet<> ( to.getEstablishments () ) );
			return person;
		}
	};

	public static final Transfer<ConferenceBean, Conference> CONFERENCE_TRANSFER
			= new Transfer<ConferenceBean, Conference> () {
		@Override
		public Conference fromStorage ( final ConferenceBean storage ) {
			Conference conference = new Conference ();
			conference.setId ( storage.getId () );
			conference.setTitle ( storage.getTitle () );
			conference.setKey ( storage.getKey () );
			conference.setKeywords ( storage.getKeywords () );
			conference.setAcronym ( storage.getAcronym () );
			conference.sethIndex ( storage.getHindex () );
			conference.setPublicationCount ( storage.getPublicationCount () );
			conference.setUrl ( storage.getUrl () );
			return conference;
		}

		@Override
		public ConferenceBean fromTransferObject ( final Conference to ) {
			ConferenceBean storage = new ConferenceBean ();
			storage.setId ( to.getId () );
			storage.setKey ( to.getKey () );
			storage.setTitle ( to.getTitle () );
			storage.setAcronym ( to.getAcronym () );
			storage.setKeywords ( to.getKeywords () );
			storage.setHindex ( to.gethIndex () );
			storage.setPublicationCount ( to.getPublicationCount () );
			storage.setUrl ( to.getUrl () );
			return storage;
		}
	};

	public static final Transfer<PluginBean, Plugin> PLUGIN_TRANSFER
			= new Transfer<PluginBean, Plugin> () {
		@Override
		public Plugin fromStorage ( final PluginBean storage ) {
			Plugin pb = new Plugin ();
			pb.setId ( storage.getId () );
			pb.setName ( storage.getName () );
			pb.setDescription ( storage.getDescription () );
			pb.setWebSocket ( storage.getWebSocket () );
			pb.setWebSockets ( storage.getWebSockets () );
			pb.setRegistrations ( storage.getRegistrations () );
			return pb;
		}

		@Override
		public PluginBean fromTransferObject ( final Plugin to ) {
			PluginBean pb = new PluginBean ();
			pb.setName ( to.getName () );
			pb.setDescription ( to.getDescription () );
			pb.setWebSocket ( to.getWebSocket () );
			pb.setRegistrations ( to.getRegistrations () );
			pb.setWebSockets ( to.getWebSockets () );
			pb.setId ( to.getId () );
			return pb;
		}
	};

	public static final Transfer<VenueBean, Venue> VENUE_TRANSFER
			= new Transfer<VenueBean, Venue> () {
		@Override
		public Venue fromStorage ( final VenueBean storage ) {
			Venue venue = new Venue ();
			venue.setId ( storage.getId () );
			venue.setKey ( storage.getKey () );
			venue.setTitle ( storage.getTitle () );
			venue.setStart ( storage.getStart () );
			venue.setEnd ( storage.getEnd () );
			venue.setYear ( storage.getYear () );
			venue.setLocation ( storage.getLocation () );
			return venue;
		}

		@Override
		public VenueBean fromTransferObject ( final Venue to ) {
			VenueBean storage = new VenueBean ();
			storage.setId ( to.getId () );
			storage.setKey ( to.getKey () );
			storage.setTitle ( to.getTitle () );
			storage.setYear ( to.getYear () );
			storage.setStart ( to.getStart () );
			storage.setEnd ( to.getEnd () );
			storage.setLocation ( to.getLocation () );
			return storage;
		}
	};

	public static final Transfer<PublicationBean, Publication> PUBLICATION_TRANSFER
			= new Transfer<PublicationBean, Publication> () {
		@Override
		public Publication fromStorage ( final PublicationBean storage ) {
			Publication publication = new Publication ();
			publication.setId ( storage.getId () );
			publication.setKey ( storage.getKey () );
			publication.setTitle ( storage.getTitle () );
			if ( storage.getKeywords () != null ) {
				publication.setKeywords ( storage.getKeywords () );
			}
			publication.setPublisher ( storage.getPublisher () );
			publication.setContent ( storage.getContent () );
			publication.setUrl ( storage.getUrl () );
			publication.setEditor ( storage.getEditor () );
			publication.setYear ( storage.getYear () );
			publication.setDoi ( storage.getDoi () );
			publication.setLanguage ( storage.getLanguage () );
			publication.setPages ( storage.getPages () );
			publication.setBooktitle ( storage.getBooktitle () );
			return publication;
		}

		@Override
		public PublicationBean fromTransferObject ( final Publication to ) {
			PublicationBean storage = new PublicationBean ();
			storage.setId ( to.getId () );
			storage.setTitle ( to.getTitle () );
			storage.setKey ( to.getKey () );
			storage.setKeywords ( to.getKeywords () );
			storage.setPublisher ( to.getPublisher () );
			storage.setUrl ( to.getUrl () );
			storage.setEditor ( to.getEditor () );
			storage.setYear ( to.getYear () );
			storage.setDoi ( to.getDoi () );
			storage.setLanguage ( to.getLanguage () );
			storage.setPages ( to.getPages () );
			storage.setBooktitle ( to.getBooktitle () );
			return storage;
		}
	};


}
