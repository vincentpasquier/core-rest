/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.storage;

import ch.hesso.predict.restful.Cfp;
import com.wordnik.swagger.annotations.ApiModelProperty;
import org.openrdf.annotations.Iri;

import java.util.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Iri (CfpBean.NS + "cfp")
public class CfpBean extends StorageEntity {

	/**
	 *
	 */
	public static final String NS = BASE_VOCAB_URL + "/conference/cfp#";

	public static final String SHORT = "cfp";

	public static final String SHORT_NS = "PREFIX " + SHORT + ":<" + NS + ">\n";

	private ConferenceBean conference;

	@Iri (NS + "keyword")
	private Set<String> keywords = new HashSet<> ();

	@Iri (NS + "content")
	private String content;

	@Iri (NS + "referrer")
	private Set<String> referrers = new HashSet<> ();

	@Iri (NS + "related")
	private Set<String> related = new HashSet<> ();

	@Iri (NS + "deadline")
	private Map<Cfp.Deadline, String> deadlines = new HashMap<> ();

	@Iri (NS + "origin")
	private String origin;

	@Iri (NS + "website")
	private String website;

	@Iri ( NS + "title" )
	private String title;

	@Iri ( NS + "acronym" )
	private String acronym;

	@Iri ( NS + "start" )
	private String start;

	@Iri ( NS + "end" )
	private String end;

	@Iri ( NS + "location" )
	private String location;

	@Iri ( NS + "conference" )
	public ConferenceBean getConference () {
		return conference;
	}

	@Iri (NS + "conference")
	public void setConference ( final ConferenceBean conference ) {
		this.conference = conference;
	}

	public void addConference ( final ConferenceBean conference ) {
		conference.getCfp ().add ( this );
		setConference ( conference );
	}

	public Set<String> getKeywords () {
		return keywords;
	}

	public void setKeywords ( final Set<String> keywords ) {
		this.keywords = keywords;
	}

	public String getContent () {
		return content;
	}

	public void setContent ( final String content ) {
		this.content = content;
	}

	public Set<String> getReferrers () {
		return referrers;
	}

	public void setReferrers ( final Set<String> referrers ) {
		this.referrers = referrers;
	}

	public Set<String> getRelated () {
		return related;
	}

	public void setRelated ( final Set<String> related ) {
		this.related = related;
	}

	public Map<Cfp.Deadline, String> getDeadlines () {
		return deadlines;
	}

	public void setDeadlines ( final Map<Cfp.Deadline, String> deadlines ) {
		this.deadlines = deadlines;
	}

	public String getOrigin () {
		return origin;
	}

	public void setOrigin ( final String origin ) {
		this.origin = origin;
	}

	public String getWebsite () {
		return website;
	}

	public void setWebsite ( final String website ) {
		this.website = website;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public String getAcronym () {
		return acronym;
	}

	public void setAcronym ( final String acronym ) {
		this.acronym = acronym;
	}

	public String getStart () {
		return start;
	}

	public void setStart ( final String start ) {
		this.start = start;
	}

	public String getEnd () {
		return end;
	}

	public void setEnd ( final String end ) {
		this.end = end;
	}

	public String getLocation () {
		return location;
	}

	public void setLocation ( final String location ) {
		this.location = location;
	}
}