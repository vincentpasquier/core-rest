/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.storage;

import org.openrdf.annotations.Iri;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Iri (PersonBean.NS + "person")
public class PersonBean extends StorageEntity {

	/**
	 *
	 */
	public static final String NS = BASE_VOCAB_URL + "/person#";

	public static final String SHORT = "per";

	public static final String SHORT_NS = "PREFIX " + SHORT + ":<" + NS + ">\n";

	@Iri (NS + "name")
	private String name;

	@Iri (NS + "title")
	private String title;

	@Iri (NS + "rank")
	private String rank;

	@Iri (NS + "country")
	private String country;

	@Iri (NS + "establishment")
	private Set<String> establishments = new HashSet<> ();

	@Iri (NS + "publication")
	private Set<PublicationBean> publications = new HashSet<> ();

	@Iri (NS + "conference")
	private Set<ConferenceBean> conferences = new HashSet<> ();

	public String getName () {
		return name;
	}

	public void setName ( final String name ) {
		this.name = name;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public String getRank () {
		return rank;
	}

	public void setRank ( final String rank ) {
		this.rank = rank;
	}

	public String getCountry () {
		return country;
	}

	public void setCountry ( final String country ) {
		this.country = country;
	}

	public Set<String> getEstablishments () {
		return establishments;
	}

	public void setEstablishments ( final Set<String> establishments ) {
		this.establishments = establishments;
	}

	public Set<PublicationBean> getPublications () {
		return publications;
	}

	public void setPublications ( final Set<PublicationBean> publications ) {
		this.publications = publications;
	}

	public void addPublication ( final PublicationBean publication, final boolean stop ) {
		publications.add ( publication );
		if ( !stop ) {
			publication.addAuthor ( this, true );
		}
	}

	public Set<ConferenceBean> getConferences () {
		return conferences;
	}

	public void setConferences ( final Set<ConferenceBean> conferences ) {
		this.conferences = conferences;
	}

	public void addConference ( final ConferenceBean conference ) {
		getConferences ().add ( conference );
		conference.getPcMembers ().add ( this );
	}
}
