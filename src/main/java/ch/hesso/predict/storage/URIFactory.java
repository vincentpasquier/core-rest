/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.storage;

import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public interface URIFactory<T> {

	URI createURI ( final ValueFactory vf, final T id );

	URI createURIWithId ( final ValueFactory vf, final String id );

	URIFactory<CategoryBean> CATEGORY = new URIFactory<CategoryBean> () {
		@Override
		public URI createURI ( final ValueFactory vf, final CategoryBean category ) {
			return createURIWithId ( vf, category.getId () );
		}

		@Override
		public URI createURIWithId ( final ValueFactory vf, final String id ) {
			return vf.createURI ( CategoryBean.NS, id );
		}
	};

	URIFactory<CfpBean> CFP = new URIFactory<CfpBean> () {
		@Override
		public URI createURI ( final ValueFactory vf, final CfpBean cfp ) {
			return createURIWithId ( vf, cfp.getId () );
		}

		@Override
		public URI createURIWithId ( final ValueFactory vf, final String id ) {
			return vf.createURI ( CfpBean.NS, id );
		}
	};

	URIFactory<ConferenceBean> CONFERENCE = new URIFactory<ConferenceBean> () {
		@Override
		public URI createURI ( final ValueFactory vf, final ConferenceBean conference ) {
			return createURIWithId ( vf, conference.getId () );
		}

		@Override
		public URI createURIWithId ( final ValueFactory vf, final String id ) {
			return vf.createURI ( ConferenceBean.NS, id );
		}
	};

	URIFactory<PersonBean> PERSON = new URIFactory<PersonBean> () {
		@Override
		public URI createURI ( final ValueFactory vf, final PersonBean person ) {
			return createURIWithId ( vf, person.getId () );
		}

		@Override
		public URI createURIWithId ( final ValueFactory vf, final String id ) {
			return vf.createURI ( PersonBean.NS, id );
		}
	};

	URIFactory<PublicationBean> PUBLICATION = new URIFactory<PublicationBean> () {
		@Override
		public URI createURI ( final ValueFactory vf, final PublicationBean publication ) {
			return createURIWithId ( vf, publication.getId () );
		}

		@Override
		public URI createURIWithId ( final ValueFactory vf, final String id ) {
			return vf.createURI ( PublicationBean.NS, id );
		}
	};

	URIFactory<VenueBean> VENUE = new URIFactory<VenueBean> () {
		@Override
		public URI createURI ( final ValueFactory vf, final VenueBean venue ) {
			return createURIWithId ( vf, venue.getId () );
		}

		@Override
		public URI createURIWithId ( final ValueFactory vf, final String id ) {
			return vf.createURI ( VenueBean.NS, id );
		}
	};
}
