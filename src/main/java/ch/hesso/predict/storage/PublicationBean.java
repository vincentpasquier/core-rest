/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.storage;

import org.openrdf.annotations.Iri;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Iri (PublicationBean.NS + "publication")
public class PublicationBean extends StorageEntity {

	/**
	 *
	 */
	public static final String NS = BASE_VOCAB_URL + "/publication#";

	public static final String SHORT = "pub";

	public static final String SHORT_NS = "PREFIX " + SHORT + ":<" + NS + ">\n";

	@Iri (NS + "title")
	private String title;

	@Iri (NS + "key")
	private String key;

	@Iri (NS + "author")
	private Set<PersonBean> authors = new HashSet<> ();

	@Iri (NS + "venue")
	private VenueBean venue;

	@Iri (NS + "abstract")
	private String content;

	@Iri (NS + "url")
	private String url;

	@Iri (NS + "editor")
	private String editor;

	@Iri (NS + "year")
	private int year;

	@Iri (NS + "doi")
	private String doi;

	@Iri (NS + "language")
	private String language;

	@Iri (NS + "keywords")
	private Set<String> keywords;

	@Iri ( NS + "publisher" )
	private String publisher;

	@Iri (NS + "pages")
	private String pages;

	@Iri (NS + "booktitle")
	private String booktitle;

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public String getKey () {
		return key;
	}

	public void setKey ( final String key ) {
		this.key = key;
	}

	public Set<PersonBean> getAuthors () {
		return authors;
	}

	public void setAuthors ( final Set<PersonBean> authors ) {
		this.authors = authors;
	}


	public String getLanguage () {
		return language;
	}

	public void setLanguage ( final String language ) {
		this.language = language;
	}

	public Set<String> getKeywords () {
		return keywords;
	}

	public void setKeywords ( final Set<String> keywords ) {
		this.keywords = keywords;
	}

	public VenueBean getVenue () {
		return venue;
	}

	public void setVenue ( final VenueBean venue ) {
		this.venue = venue;
	}


	public String getContent () {
		return content;
	}

	public void setContent ( final String content ) {
		this.content = content;
	}

	public String getUrl () {
		return url;
	}

	public void setUrl ( final String url ) {
		this.url = url;
	}

	public String getEditor () {
		return editor;
	}

	public void setEditor ( final String editor ) {
		this.editor = editor;
	}

	public int getYear () {
		return year;
	}

	public void setYear ( final int year ) {
		this.year = year;
	}

	public String getDoi () {
		return doi;
	}

	public void setDoi ( final String doi ) {
		this.doi = doi;
	}

	public String getPublisher () {
		return publisher;
	}

	public void setPublisher ( final String publisher ) {
		this.publisher = publisher;
	}

	public String getPages () {
		return pages;
	}

	public void setPages ( final String pages ) {
		this.pages = pages;
	}

	public String getBooktitle () {
		return booktitle;
	}

	public void setBooktitle ( final String booktitle ) {
		this.booktitle = booktitle;
	}

	public void addAuthor ( final PersonBean author, final boolean stop ) {
		authors.add ( author );
		if ( !stop ) {
			author.addPublication ( this, true );
		}
		//author.getPublications ().add ( this );
		//getAuthors ().add ( author );
	}

	public void addVenue ( final VenueBean venue ) {
		setVenue ( venue );
		venue.getPublications ().add ( this );
	}
}
