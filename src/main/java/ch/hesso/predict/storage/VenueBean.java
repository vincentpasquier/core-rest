/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.storage;

import org.openrdf.annotations.Iri;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Iri ( VenueBean.NS + "venue" )
public class VenueBean extends StorageEntity {

	/**
	 *
	 */
	public static final String NS = BASE_VOCAB_URL + "/conference/venue#";

	public static final String SHORT = "ven";

	public static final String SHORT_NS = "PREFIX " + SHORT + ":<" + NS + ">\n";

	@Iri ( NS + "conference" )
	private ConferenceBean conference;

	@Iri ( NS + "location" )
	private Set<String> location;

	@Iri ( NS + "start" )
	private Date start;

	@Iri ( NS + "end" )
	private Date end;

	@Iri ( NS + "year" )
	private int year;

	@Iri ( NS + "publication" )
	private Set<PublicationBean> publications = new HashSet<> ();

	@Iri ( NS + "key" )
	private String key;

	@Iri ( NS + "title" )
	private String title;

	public ConferenceBean getConference () {
		return conference;
	}

	public void setConference ( final ConferenceBean conference ) {
		this.conference = conference;
	}

	public void addConference ( final ConferenceBean conference ) {
		conference.getVenues ().add ( this );
		setConference ( conference );
	}

	public Set<String> getLocation () {
		return location;
	}

	public void setLocation ( final Set<String> location ) {
		this.location = location;
	}

	public Date getStart () {
		return start;
	}

	public void setStart ( final Date start ) {
		this.start = start;
	}

	public Date getEnd () {
		return end;
	}

	public void setEnd ( final Date end ) {
		this.end = end;
	}

	public Set<PublicationBean> getPublications () {
		return publications;
	}

	public void setPublications ( final Set<PublicationBean> publications ) {
		this.publications = publications;
	}

	public void addPublication ( final PublicationBean publication ) {
		publication.setVenue ( this );
		getPublications ().add ( publication );
	}

	public int getYear () {
		return year;
	}

	public void setYear ( final int year ) {
		this.year = year;
	}

	public String getKey () {
		return key;
	}

	public void setKey ( final String key ) {
		this.key = key;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}
}
