/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.storage;

import org.openrdf.annotations.Iri;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@Iri (ConferenceBean.NS + "conference")
public class ConferenceBean extends StorageEntity {

	/**
	 *
	 */
	public static final String NS = BASE_VOCAB_URL + "/conference#";

	public static final String SHORT = "conf";

	public static final String SHORT_NS = "PREFIX " + SHORT + ":<" + NS + ">\n";

	@Iri (NS + "key")
	private String key;

	@Iri (NS + "title")
	private String title;

	@Iri (NS + "acronym")
	private String acronym;

	@Iri (NS + "cfp")
	private Set<CfpBean> cfp = new HashSet<> ();

	@Iri (NS + "venue")
	private Set<VenueBean> venues = new HashSet<> ();

	@Iri (NS + "pcMember")
	private Set<PersonBean> pcMembers = new HashSet<> ();

	@Iri (NS + "category")
	private Set<CategoryBean> categories = new HashSet<> ();

	@Iri (NS + "cat")
	private Set<String> category;

	@Iri (NS + "keywords")
	private Set<String> keywords = new HashSet<> ();

	@Iri (NS + "h-index")
	private int hindex;

	@Iri (NS + "publicationCount")
	private int publicationCount;

	@Iri (NS + "url")
	private String url;

	public String getKey () {
		return key;
	}

	public void setKey ( final String key ) {
		this.key = key;
	}

	public Set<CfpBean> getCfp () {
		return cfp;
	}

	public void setCfp ( final Set<CfpBean> cfp ) {
		this.cfp = cfp;
	}

	public void addCfp ( final CfpBean call ) {
		getCfp ().add ( call );
		call.setConference ( this );
	}

	public Set<VenueBean> getVenues () {
		return venues;
	}

	public void setVenues ( final Set<VenueBean> venues ) {
		this.venues = venues;
	}

	public void addVenue ( final VenueBean venue ) {
		getVenues ().add ( venue );
		venue.setConference ( this );
	}

	public Set<PersonBean> getPcMembers () {
		return pcMembers;
	}

	public void setPcMembers ( final Set<PersonBean> pcMembers ) {
		this.pcMembers = pcMembers;
	}

	public void addPcMember ( final PersonBean pcMember ) {
		getPcMembers ().add ( pcMember );
		pcMember.getConferences ().add ( this );
	}

	public Set<String> getCategory () {
		return category;
	}

	public void setCategory ( final Set<String> category ) {
		this.category = category;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public String getAcronym () {
		return acronym;
	}

	public void setAcronym ( final String acronym ) {
		this.acronym = acronym;
	}

	public String getUrl () {
		return url;
	}

	public void setUrl ( final String url ) {
		this.url = url;
	}

	public Set<String> getKeywords () {
		return keywords;
	}

	public void setKeywords ( final Set<String> keywords ) {
		this.keywords = keywords;
	}

	public Set<CategoryBean> getCategories () {
		return categories;
	}

	public void setCategories ( final Set<CategoryBean> categories ) {
		this.categories = categories;
	}

	public void addCategory ( final String category ) {
		getKeywords ().add ( category );
	}

	public void addCategories ( final Set<String> category ) {
		getKeywords ().addAll ( category );
	}

	public int getHindex () {
		return hindex;
	}

	public void setHindex ( final int hindex ) {
		this.hindex = hindex;
	}

	public int getPublicationCount () {
		return publicationCount;
	}

	public void setPublicationCount ( final int publicationCount ) {
		this.publicationCount = publicationCount;
	}
}
