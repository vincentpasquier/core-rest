'use strict';

angular.module ( 'MasterWebsite', [
		'chieffancypants.loadingBar',
		'ngAnimate',
		'ngRoute',
		'ngSanitize',
		'ui.bootstrap',
		'decipher.tags',
		'WebSockets',
		'CFP',
		'Conferences',
		'People',
		'Plugins',
		'Publications',
		'Recommend',
		'Venues'
	] )
	.config ( function ( $routeProvider ) {
	$routeProvider
		.when ( '/', {
		templateUrl: 'views/main.html'
	} )
		.when ( '/parameters', {
		templateUrl: 'views/recommendation/parameters.html', controller: 'ParametersCtrl'
	} )
		.when ( '/recommendation', {
		templateUrl: 'views/recommendation/recommendation.html', controller: 'RecommendationCtrl'
	} )
		.when ( '/resources', {
		templateUrl: 'views/resources.html'
	} )
		.when ( '/plugins', {
		templateUrl: 'views/plugins/plugins.html', controller: 'PluginsCtrl'
	} )
		.when ( '/people', {
		templateUrl: 'views/people/people.html', controller: 'PeopleCtrl'
	} )
		.when ( '/people/:peopleId', {
		templateUrl: 'views/people/person.html', controller: 'PersonCtrl'
	} )
		.when ( '/conferences', {
		templateUrl: 'views/conferences/conferences.html', controller: 'ConferencesCtrl'
	} )
		.when ( '/conferences/:conferenceId', {
		templateUrl: 'views/conferences/conference.html', controller: 'ConferenceCtrl'
	} )
		.when ( '/publications', {
		templateUrl: 'views/publications/publications.html', controller: 'PublicationsCtrl'
	} )
		.when ( '/publications/:publicationId', {
		templateUrl: 'views/publications/publication.html', controller: 'PublicationCtrl'
	} )
		.when ( '/cfp', {
		templateUrl: 'views/cfp/cfps.html', controller: 'CFPsCtrl'
	} )
		.when ( '/cfp/:cfpId', {
		templateUrl: 'views/cfp/cfp.html', controller: 'CFPCtrl'
	} )
		.when ( '/venues', {
		templateUrl: 'views/venues/venues.html', controller: 'VenuesCtrl'
	} )
		.when ( '/venues/:venueId', {
		templateUrl: 'views/venues/venue.html', controller: 'VenueCtrl'
	} )
		.otherwise ( {
		redirectTo: '/'
	} );
} )
	.controller ( 'MainCtrl', [ '$scope', '$location', 'TitleFactory', function ( $scope, $location, TitleFactory ) {

		$scope.location = $location;

		$scope.titleShow = true;

		$scope.titleStyle = '';

		$scope.TitleFactory = TitleFactory;

		$scope.$watch ( 'location.$$path', function ( e ) {
			$scope.titleShow = e !== '/';
		} );

		function capitaliseFirstLetter ( string ) {
			return string.charAt ( 0 ).toUpperCase () + string.slice ( 1 );
		}

		$scope.$on ( '$routeChangeSuccess', function ( e, n ) {
			var title = n.$$route.originalPath;
			title = capitaliseFirstLetter ( title.replace ( /\//g, '' ) );
			var subStr = title.indexOf ( ':' );
			if ( subStr != -1 ) {
				title = title.substr ( 0, subStr );
			}
			TitleFactory.setTitle ( title );
		} );

	}] )
	.factory ( 'TitleFactory', [ '$log', function ( $log ) {
		var prefix = 'PREDICT';
		return {
			title   : '',
			getTitle: function () {
				return this.title;
			},
			setTitle: function ( title ) {
				this.title = prefix + ( title.length == 0 ? '' : ' - ' + title );
			}
		};
	} ] );