'use strict';

angular.module ( 'Venues', [ 'REST' ] )
	.controller ( 'VenueCtrl', [ '$scope', '$routeParams', 'VenueResources', function ( $scope, $routeParams, VenueResources ) {

		$scope.data = {};
		$scope.venueId = $routeParams.venueId;

		VenueResources.getById ( $scope.venueId ).then ( function ( data ) {
			$scope.data = data;
		} );

		VenueResources.getPublications ( $scope.venueId ).then ( function ( data ) {
			$scope.data.publications = data;
		} );

		VenueResources.getAuthors ( $scope.venueId ).then ( function ( data ) {
			$scope.data.authors = data;
		} );

	}] )
	.controller ( 'VenuesCtrl', [ '$scope', '$location', 'VenueResources', function ( $scope, $location, VenueResources ) {

		$scope.data = [];
		$scope.page = 0;
		$scope.search = $location.search ();

		$scope.changePage = function ( page ) {
			page = parseInt(page);
			VenueResources.getAll ( page ).then ( function ( data ) {
				$scope.data = data;
				$scope.page = page;
				$location.path ( $location.path () ).search ( {page: page} );
			} );
		}

		if ( !$scope.search.page ) {
			$scope.changePage ( $scope.page );
		} else {
			$scope.changePage ( $scope.search.page );
		}

	}] )
	.factory ( 'VenueResources', [ '$http', 'resources', function ( $http, resources ) {
		var all = {};
		var ids = {};
		var publications = {};
		var authors = {};
		var venue = {
			getAll         : function ( page ) {
				if ( !page ) {
					page = 0;
				}
				if ( !all[page] ) {
					all[page] = $http.get ( resources.venues + "?page=" + page ).then ( function ( response ) {
						return response.data;
					} );
				}
				return all[page];
			},
			getById        : function ( id ) {
				if ( id ) {
					if ( !ids[id] ) {
						ids[id] = $http.get ( resources.venues + "/" + id ).then ( function ( response ) {
							return response.data;
						} );
					}
					return ids[id];
				}
			},
			getPublications: function ( id ) {
				if ( id ) {
					if ( !publications[id] ) {
						publications[id] = $http.get ( resources.venues + "/" + id + "/publications" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return publications[id];
				}
			},
			getAuthors     : function ( id ) {
				if ( id ) {
					if ( !authors[id] ) {
						authors[id] = $http.get ( resources.venues + "/" + id + "/authors" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return authors[id];
				}
			}
		};
		return venue;
	}] );