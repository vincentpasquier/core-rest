'use strict';

angular.module ( 'Publications', [ 'REST' ] )
	.controller ( 'PublicationCtrl', [ '$scope', '$routeParams', 'PublicationResource', function ( $scope, $routeParams, PublicationResource ) {

		$scope.data = {};
		$scope.publicationId = $routeParams.publicationId;

		PublicationResource.getById ( $scope.publicationId ).then ( function ( data ) {
			$scope.data = data;
		} );

		PublicationResource.getVenue ( $scope.publicationId ).then ( function ( data ) {
			$scope.data.venue = data;
		} );

		PublicationResource.getAuthors ( $scope.publicationId ).then ( function ( data ) {
			$scope.data.authors = data;
		} );

	}] )
	.controller ( 'PublicationsCtrl', [ '$scope', '$location', 'PublicationResource', function ( $scope, $location, PublicationResource ) {

		$scope.data = [];
		$scope.page = 0;
		$scope.search = $location.search ();

		$scope.changePage = function ( page ) {
			page = parseInt(page);
			PublicationResource.getAll ( page ).then ( function ( data ) {
				$scope.data = data;
				$scope.page = page;
				$location.path ( $location.path () ).search ( {page: page} );
			} );
		}

		if ( !$scope.search.page ) {
			$scope.changePage ( $scope.page );
		} else {
			$scope.changePage ( $scope.search.page );
		}

	}] )
	.factory ( 'PublicationResource', [ '$http', 'resources', function ( $http, resources ) {
		var all = {};
		var ids = {};
		var authors = {};
		var venue = {};
		var publication = {
			getAll    : function ( page ) {
				if ( !page ) {
					page = 0;
				}
				if ( !all[page] ) {
					all[page] = $http.get ( resources.publications + "?page=" + page ).then ( function ( response ) {
						return response.data;
					} );
				}
				return all[page];
			},
			getById   : function ( id ) {
				if ( id ) {
					if ( !ids[id] ) {
						ids[id] = $http.get ( resources.publications + "/" + id ).then ( function ( response ) {
							return response.data;
						} );
					}
					return ids[id];
				}
			},
			getVenue  : function ( id ) {
				if ( id ) {
					if ( !venue[id] ) {
						venue[id] = $http.get ( resources.publications + "/" + id + "/venue" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return venue[id];
				}
			},
			getAuthors: function ( id ) {
				if ( id ) {
					if ( !authors[id] ) {
						authors[id] = $http.get ( resources.publications + "/" + id + "/authors" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return authors[id];
				}
			}
		};
		return publication;
	}] );