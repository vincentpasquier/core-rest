'use strict';

angular.module ( 'Plugins', [ 'REST' ] )
	.controller ( 'PluginsCtrl', [ '$scope', 'PluginResource', 'PluginsNotification', function ( $scope, service, notification ) {

		/**
		 *
		 */
		service.request ().then ( function ( data ) {
			$scope.data = data;
		} );

		/**
		 *
		 */
		notification.webSocket ().then ( function ( websocket ) {
			websocket.onmessage = function ( msg ) {
				var data = angular.fromJson ( msg.data );
				if ( data.method === 'DELETE' ) {
					$scope.data = $scope.data.filter ( function ( element ) {
						return element.name !== data.name;
					} );
				} else {
					$scope.data.push ( data );
				}
				$scope.$apply ();
			};
		} );

		/**
		 *
		 */
		$scope.$on ( '$destroy', function () {
			angular.forEach ( $scope.data, function ( e ) {
				if ( e.ws ) {
					e.ws.close ();
				}
			} );
		} );

		$scope.connect = function ( plugin ) {
			if ( !plugin.ws ) {
				plugin.ws = new WebSocket ( plugin.webSocket );
				plugin.ws.onmessage = function ( msg ) {
					plugin.connected = true;
					var jsonMsg = angular.fromJson ( msg.data );
					$scope.$apply ( function () {
						if ( jsonMsg.state ) {
							plugin.state = jsonMsg;
							var percent = 100;
							if ( plugin.state.totalProgress != 0 ) {
								percent = ( 100 * plugin.state.progress ) / plugin.state.totalProgress;
							}
							var subPercent = 100;
							if ( plugin.state.totalSubProgress != 0 ) {
								var subPercent = ( 100 * plugin.state.subProgress ) / plugin.state.totalSubProgress;
							}
							$scope.progressStyle = 'width:' + percent + '%'
							$scope.progressSubStyle = 'width:' + subPercent + '%'
						} else {
							plugin.params = jsonMsg;
						}
					} );
				}
				plugin.ws.onclose = function ( msg ) {
					plugin.connected = false;
				}
			}
		}

		$scope.disconnect = function ( plugin ) {
			if ( plugin.ws ) {
				plugin.ws.close ();
				plugin.connected = false;
				delete plugin.ws;
			}
		}

		$scope.execute = function ( plugin, method ) {
			var query = {
				execute : method,
				booleans: {},
				doubles : {},
				lists   : {},
				longs   : {},
				strings : {}
			};
			angular.forEach ( plugin.params.booleans, function ( e ) {
				query.booleans[e.name] = e.value;
			} );
			angular.forEach ( plugin.params.doubles, function ( e ) {
				query.doubles[e.name] = e.value;
			} );
			angular.forEach ( plugin.params.lists, function ( e ) {
				query.lists[e.name] = e.value.split ( ',' );
			} );
			angular.forEach ( plugin.params.longs, function ( e ) {
				query.longs[e.name] = e.value;
			} );
			angular.forEach ( plugin.params.strings, function ( e ) {
				query.strings[e.name] = e.value;
			} );
			plugin.ws.send ( JSON.stringify ( query ) );
		}

	}] )
	.factory ( 'PluginResource', [ '$http', 'resources', function ( $http, resources ) {
		var promise;
		var plugin;
		var service = {
			request: function () {
				if ( !promise ) {
					promise = $http.get ( resources.plugins ).then ( function ( response ) {
						return response.data;
					} );
				}
				return promise;
			},
			post   : function ( data ) {
				if ( !plugin ) {
					plugin = $http.post ( resources.plugins, data ).then ( function ( response ) {
						return response.data;
					} )
				}
				return plugin;
			}
		};
		return service;
	}] )
	.factory ( 'PluginsNotification', [ 'PluginResource', '$q', function ( resource, $q ) {
		var defer = $q.defer ();
		var websocket;
		var plugin = {
			"name"         : "website-",
			"registrations": {
				"plugin": ["POST", "PUT", "DELETE"]
			}
		};

		function generateUIDNotMoreThan1million () {
			return ("00000000" + (Math.random () * Math.pow ( 36, 8 ) << 0).toString ( 36 )).substr ( -8 )
		}

		var notification = {
			webSocket: function () {
				if ( !websocket ) {
					plugin.name += generateUIDNotMoreThan1million ();
					resource.post ( plugin ).then ( function ( data ) {
						websocket = new WebSocket ( data.webSockets.plugin );
						defer.resolve ( websocket );
					} );
				}
				return defer.promise;
			}
		};
		return notification;
	}] );