'use strict';

angular.module ( 'Recommend', [ 'REST', 'Plugins', 'nvd3ChartDirectives' ] )
	.controller ( 'RecommendationCtrl', [ '$scope', '$http', '$location', 'resources', 'Recommendation', 'PluginResource', 'Store', function ( $scope, $http, $location, resources, Recommendation, PluginResource, Store ) {

		var SEARCHED = "241bd687"; // topic plugin ID

		$scope.Store = Store;

		$scope.topic = 0;

		$scope.words = [];

		$scope.changeTopic = function ( topic ) {
			$scope.topic = topic;
			$scope.words = $scope.Store.result[topic].words;
		}

		$scope.$watch ( 'Store.plugin', function ( e ) {
			if ( e && e.webSocket ) {
				if ( !$scope.Store.ws ) {
					$scope.Store.ws = new WebSocket ( e.webSocket );
					$scope.Store.ws.onmessage = function ( msg ) {
						var jsonMsg = angular.fromJson ( msg.data );
						$scope.$apply ( function () {
							if ( jsonMsg.state === 'DONE' ) {
								$scope.Store.result = jsonMsg.objects.result;
								$scope.Store.nd3 = [];
                                $scope.Store.all = [];
								angular.forEach ( $scope.Store.result, function ( e ) {
									var toPush = [];
									var build = {
										key   : e.topic,
										values: e.years
									};
									toPush.push ( build );
                                    $scope.Store.all.push ( build );
									$scope.Store.nd3.push ( toPush );
									$scope.words = $scope.Store.result[$scope.topic].words;
								} );
								Recommendation.run = false;
							} else {
								var percent = 100;
								if ( jsonMsg.totalProgress != 0 ) {
									percent = ( 100 * jsonMsg.progress ) / jsonMsg.totalProgress;
								}
								var subPercent = 100;
								if ( jsonMsg.totalSubProgress != 0 ) {
									var subPercent = ( 100 * jsonMsg.subProgress ) / jsonMsg.totalSubProgress;
								}
								$scope.progressStyle = 'width:' + percent + '%';
								$scope.progressSubStyle = 'width:' + subPercent + '%';
								$scope.Store.status = jsonMsg;
							}
						} );
					}
					$scope.Store.ws.onopen = function ( connect ) {
						$scope.connect ();
					}
				}
			}
		} );

		$scope.connect = function () {
			var query = {
				execute: 'performTopicAnalysis',
				lists  : {},
				longs  : {}
			};
			query.lists.acronyms = Recommendation.conferenceAcronym;
			query.lists.keywords = Recommendation.keywords;
			query.lists.titles = Recommendation.conferenceTitle;
			query.longs.startYear = Recommendation.startYear;
			query.longs.endYear = Recommendation.endYear;
			query.longs.topicNumber = Recommendation.topicNumber;
			if ( Recommendation.run ) {
				$scope.Store.ws.send ( JSON.stringify ( query ) );
				Recommendation.committee ().then ( function ( committee ) {
					$scope.Store.committee = committee;
				} );
			} else {
				$location.path ( 'recommendation' );
			}
		};

		PluginResource.request ().then ( function ( plugins ) {
			var plugin = plugins.filter ( function ( el ) {
				return el.id === SEARCHED;
			} );
			$scope.Store.plugin = plugin[0];
		} );

	}] )
	.
	controller ( 'ParametersCtrl', [ '$scope', '$http', '$location', 'resources', 'Recommendation', function ( $scope, $http, $location, resources, Recommendation ) {

		$scope.tags = [];

		$scope.location = $location;

        $scope.activeConferences = true;

        $scope.activeKeywords = true;

		$scope.getTypeAhead = function ( val, resource ) {
			var promise = $http.get ( resources.recommendations + '/' + resource, {
				params: {
					query: val
				}
			} ).then ( function ( res ) {
				return res.data;
			} );
			promise.$$v = promise;
			return promise;
		};

		$scope.submitTitles = function () {
			$scope.tags.push ( { type: 'conferenceTitle', text: $scope.titles, color: 'label-primary', icon: 'glyphicon-book' } );
			$scope.titles = '';
		};

		$scope.submitAcronyms = function () {
			$scope.tags.push ( { type: 'conferenceAcronym', text: $scope.acronyms, color: 'label-success', icon: 'glyphicon-bookmark' } );
			$scope.acronyms = '';
		};

		$scope.submitKeywords = function () {
			$scope.tags.push ( { type: 'keywords', text: $scope.keywords, color: 'label-warning', icon: 'glyphicon-tags' } );
			$scope.keywords = '';
		};

		$scope.remove = function ( remove ) {
			$scope.tags = $scope.tags.filter ( function ( el ) {
				return el !== remove;
			} );
		};

		$scope.recommendation = function () {
			angular.forEach ( $scope.tags, function ( e ) {
				if ( e.type && Recommendation[e.type] ) {
					Recommendation[e.type].push ( e.text );
				}
			} );
			Recommendation.run = true;
			if ( $scope.startYear ) {
				Recommendation.startYear = $scope.startYear ;
			}
			if ( $scope.endYear ) {
				Recommendation.endYear = $scope.endYear;
			}
			Recommendation.topicNumber = $scope.topicModel;
			$location.path ( 'recommendation' );
		}

	}] )
	.factory ( 'Store', [ function () {
		var store = {
			plugin: {},
			status: {}
		};
		return store;
	}] )
	.factory ( 'Recommendation', [ '$http', 'resources', function ( $http, resources ) {
		var request;
		var recommendation = {
			conferenceAcronym: [],
			keywords         : [],
			conferenceTitle  : [],
			startYear        : 2000,
			endYear          : 2014,
			topicNumber      : 10,
			run              : false,
			committee        : function () {
				if ( !request ) {
					var rec = {};
					rec.bulk = false;
					rec.hasMore = false;
					rec.conferenceAcronym = recommendation.conferenceAcronym;
					rec.keywords = recommendation.keywords;
					rec.conferenceTitle = recommendation.conferenceTitle;
					rec.startYear = recommendation.startYear;
					rec.endYear = recommendation.endYear;
					request = $http.post ( resources.recommendations + "/committee", rec ).then ( function ( response ) {
						return response.data;
					} );
				}
				return request;
			}
		};
		return recommendation;
	}] );