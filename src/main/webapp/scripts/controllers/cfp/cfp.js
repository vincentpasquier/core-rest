'use strict';

angular.module ( 'CFP', [ 'REST' ] )
	.controller ( 'CFPCtrl', [ '$scope', '$routeParams', 'CFPResource', function ( $scope, $routeParams, CFPResource ) {

		$scope.data = {};
		$scope.cfpId = $routeParams.cfpId;

		CFPResource.getById ( $scope.cfpId ).then ( function ( data ) {
			$scope.data = data;
			console.log ( data );
		} );

		CFPResource.getConference ( $scope.cfpId ).then ( function ( data ) {
			$scope.data.conference = data;
			console.log ( data );
		} );

	}] )
	.controller ( 'CFPsCtrl', [ '$scope', '$location', 'CFPResource', function ( $scope, $location, CFPResource ) {

		$scope.data = [];
		$scope.page = 0;
		$scope.search = $location.search ();

		$scope.changePage = function ( page ) {
			page = parseInt ( page );
			CFPResource.getAll ( page ).then ( function ( data ) {
				$scope.data = data;
				$scope.page = page;
				$location.path ( $location.path () ).search ( {page: page} );
			} );
		}

		if ( !$scope.search.page ) {
			$scope.changePage ( $scope.page );
		} else {
			$scope.changePage ( $scope.search.page );
		}
	}] )
	.factory ( 'CFPResource', [ '$http', 'resources', function ( $http, resources ) {
		var all = {};
		var ids = {};
		var conferences = {};
		var cfp = {
			getAll       : function ( page ) {
				if ( !page ) {
					page = 0;
				}
				if ( !all[page] ) {
					all[page] = $http.get ( resources.cfp + "?page=" + page ).then ( function ( response ) {
						return response.data;
					} );
				}
				return all[page];
			},
			getById      : function ( id ) {
				if ( id ) {
					if ( !ids[id] ) {
						ids[id] = $http.get ( resources.cfp + "/" + id ).then ( function ( response ) {
							return response.data;
						} );
					}
					return ids[id];
				}
			},
			getConference: function ( id ) {
				if ( id ) {
					if ( !conferences[id] ) {
						conferences[id] = $http.get ( resources.cfp + "/" + id + "/conference" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return conferences[id];
				}
			}
		};
		return cfp;
	}] )
	.filter ( 'newlines', function () {
		return function ( text ) {
			return text.replace ( /\n/g, '<br/>' );
		}
	} )
	.filter ( 'noHTML', function () {
		return function ( text ) {
			return text
				.replace ( /&/g, '&amp;' )
				.replace ( />/g, '&gt;' )
				.replace ( /</g, '&lt;' );
		}
	} );